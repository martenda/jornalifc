<?php

require_once 'Database.php';

function listarNoticias($categoria)
{
  $conn = Database::getConnection();

  $consulta = $conn->query("SELECT id_noticia, titulo_noticia, texto_noticia, cod_categoria FROM noticias
                            WHERE cod_categoria = '$categoria' ORDER BY id_noticia DESC");
  $noticias = $consulta->fetchAll(PDO::FETCH_ASSOC);

  return $noticias;
}

function obterNoticia($titulo)
{
  $conn = Database::getConnection();

  $consulta = $conn->query("SELECT id_noticia, texto_noticia, tags_referencia, cod_categoria, id_usuario
                            FROM noticias WHERE titulo_noticia = '$titulo' ");
  $noticia = $consulta->fetchAll(PDO::FETCH_ASSOC);

  return $noticia[0];
}

function obterAutorNoticia($id_usuario)
{
    $conn = Database::getConnection();

    //$consulta = $conn->query("SELECT nome FROM usuario WHERE id_usuario = $id_usuario");
    $consulta = $conn->query("SELECT obterAutorNoticia($id_usuario)");
    $nome = $consulta->fetchAll(PDO::FETCH_NUM);

    return $nome[0][0];
}

function obterImagensNoticia($id_noticia)
{
  $conn = Database::getConnection();

  $consulta = $conn->query("SELECT cod_midia, caminho, titulo FROM midia WHERE id_noticia = $id_noticia");
  $imagens = $consulta->fetchAll(PDO::FETCH_ASSOC);

  return $imagens;
}

function obterNoticiaPesquisada($id_noticia)
{
  $conn = Database::getConnection();

  $consulta = $conn->query("SELECT id_noticia, titulo_noticia, texto_noticia, tags_referencia, cod_categoria
                            FROM noticias WHERE id_noticia = '$id_noticia' ");
  $noticia = $consulta->fetchAll(PDO::FETCH_ASSOC);

  return $noticia[0];
}
