<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php
session_start();

require_once 'permite_acesso.php';
permiteAcessoAdmin();

$id_usuario = filter_input(INPUT_GET, 'id_usuario');

require_once 'Database.php';
require_once 'valida_dados.php';

$id_usuario = validaDados($id_usuario, "ID do Usuário");

if ($id_usuario != false){

      $conn = Database::getConnection();

      $banco = "UPDATE usuario SET id_tipo_usuario = 1
                WHERE id_usuario = '$id_usuario'";
      $conn->exec($banco);

      echo("
            <script type='text/javascript'>
            location.href='../visao/home.php?navegacao=ver_usuarios';
            </script>");

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'A URL está incorreta! Tente novamente.',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../visao/home.php?navegacao=ver_usuarios';
            }
        );
        </script>");
}
