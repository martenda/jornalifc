<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php
require_once 'permite_acesso.php';
permiteAcessoAdmin();

session_start();

$id_usuario = $_SESSION['usuario']['id'];

require_once 'Database.php';
require_once 'valida_dados.php';

$id_usuario = validaDados($id_usuario, "ID do Usuário");

if ($id_usuario != false){

      $conn = Database::getConnection();

      $banco = "UPDATE usuario SET id_tipo_usuario = 2
                WHERE id_usuario = '$id_usuario'";
      $conn->exec($banco);

      $_SESSION['usuario']['admin'] = 0;

      echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Sucesso!',
                  text: 'Você deixou de ser admin! Vlw :)',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../visao/home.php';
              }
          );
          </script>");

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'A URL está incorreta! Tente novamente.',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../visao/home.php?navegacao=detalhes_da conta';
            }
        );
        </script>");
}
