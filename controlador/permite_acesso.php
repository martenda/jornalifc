<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php

function permiteAcessoUsuario(){

    if ( !isset($_SESSION['usuario']) OR $_SESSION['usuario']['esta_logado'] == false ) {
        echo("<br>
            <script type='text/javascript'>
            swal(
                {
                    title: 'Erro!',
                    text: 'Você não está logado! Não tem permissão para acessar essa página. Faça o login antes...',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonClass: 'btn-succes',
                    confirmButtonText: 'Ok'
                },
                function(){
                    location.href='../visao/home.php?navegacao=login';
                }
            );
            </script>");
    }
}

function permiteAcessoAdmin(){

    if ( !isset($_SESSION['usuario']) OR $_SESSION['usuario']['admin'] == false ) {
        echo("<br>
            <script type='text/javascript'>
            swal(
                {
                    title: 'Erro!',
                    text: 'Você não é um administrador! Não tem permissão para acessar essa página. Faça o login antes...',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonClass: 'btn-succes',
                    confirmButtonText: 'Ok'
                },
                function(){
                    location.href='../visao/home.php?navegacao=login';
                }
            );
            </script>");
    }
}
