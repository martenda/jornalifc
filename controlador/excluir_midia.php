<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php
require_once 'permite_acesso.php';
permiteAcessoAdmin();

$cod_midia = filter_input(INPUT_GET, 'cod_midia');

require_once 'Database.php';
require_once 'valida_dados.php';

$cod_midia = validaDados($cod_midia, "Código da mídia");

if ($cod_midia != false){

      $conn = Database::getConnection();

      // Pega o titulo do arquivo no banco
      $consulta_midia = "SELECT titulo FROM midia WHERE cod_midia = '$cod_midia' ";
      $stmt = $conn->query($consulta_midia);
      $resultado = $stmt->fetch(PDO::FETCH_ASSOC);
      $titulo_midia = $resultado['titulo'];

      // Deleta o arquivo da pasta
      $caminho = "../visao/imgs/";
      unlink($caminho.$titulo_midia);

      // Deleta a mídia do banco
      $banco = "DELETE FROM `midia` WHERE cod_midia = $cod_midia";
      $conn->exec($banco);

      echo "<br><br>A mídia está sendo excluída...";

      echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Sucesso!',
                  text: 'A mídia foi excluida com sucesso! Vlw :)',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../visao/home.php';
              }
          );
          </script>");

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'A URL está incorreta! Tente novamente.',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../visao/home.php';
            }
        );
        </script>");
}
