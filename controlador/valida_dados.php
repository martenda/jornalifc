<?php

function validaDados($campo, $tipo_campo){

    //Checa campos vazios
    if ( empty($campo) ){
        $campo = false;
    } else {

        //Remove caracteres indesejados
        $campo = trim($campo);
        $campo = stripslashes($campo);
        $campo = htmlspecialchars($campo);

        //Valida os campos específicamente conforme o tipo
        switch ($tipo_campo) {
            case 'E-mail':
            case 'Email':
                $campo = validaEmail($campo);
                break;
            case 'CPF':
                $campo = validaCPF($campo);
                break;
            case 'Data de nascimento':
                $campo = validaData($campo);
                break;
            case 'Senha':
            case 'Confirmar senha':
            case 'Senha atual':
            case 'Nova senha':
                $campo = validaSenha($campo);
                break;
        }
    }

    return $campo;
}




function validaEmail($campo){


    return $campo;
}

function validaCPF($campo){


    return $campo;
}

function validaData($campo){


    return $campo;
}

function validaSenha($campo){

    $campo = md5($campo);

    return $campo;
}
