<?php

require_once 'Database.php';

function obterDetalhesConta($id_usuario)
{
    $conn = Database::getConnection();

    // Retorna uma declaracao: statement
    $consulta = $conn->query("SELECT nome, cpf, dt_nasc FROM usuario WHERE id_usuario = '$id_usuario' ");
    $detalhes = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $detalhes[0];
}

function calculaIdade($dt_nasc)
{
    date_default_timezone_set('America/Sao_Paulo');
    $dt_atual = date('Y-m-d');

    $idade = $dt_atual - $dt_nasc;

    return $idade;
}
