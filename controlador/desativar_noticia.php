<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php
require_once 'permite_acesso.php';
permiteAcessoAdmin();

$id_noticia = filter_input(INPUT_GET, 'id_noticia');

require_once 'Database.php';
require_once 'valida_dados.php';

$id_noticia = validaDados($id_noticia, "ID da notícia");

if ($id_noticia != false){

      $conn = Database::getConnection();

      $banco = "UPDATE noticias SET cod_categoria = 5
                WHERE id_noticia = '$id_noticia'";
      $conn->exec($banco);

      echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Sucesso!',
                  text: 'Esta notícia foi desativada! Vlw :)',
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../visao/home.php';
              }
          );
          </script>");

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'A URL está incorreta! Tente novamente.',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../../visao/home.php';
            }
        );
        </script>");
}
