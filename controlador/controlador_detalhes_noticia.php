<?php

require_once 'Database.php';

function obterDetalhesNoticia($id_noticia)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT titulo_noticia, texto_noticia, tags_referencia, cod_categoria, id_usuario FROM noticias WHERE id_noticia = '$id_noticia' ");
    $detalhes = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $detalhes[0]['texto_noticia'] = str_replace("<br />",'', $detalhes[0]['texto_noticia']);

    return $detalhes[0];
}
