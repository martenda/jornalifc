<?php

if(!isset($_SESSION))
{
    session_start();
}

require_once '../modelo/login.class.php';
include_once 'controlador_categorias.php';

?>

    <div class="container-fluid">
     <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="home.php">Jornal IFC</a>
    </div>


    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">



        <!-- fomulario de busca -->

        <form class="navbar-form navbar-left" role="search" name="sentMessage" id="cadastroForm" novalidate action="./../controlador/formularios/processa_pesquisar_noticia.php" method="get">
            <div class="form-group">
                <input type="text" name="pesquisa" class="form-control" id="pesquisa"/>
            </div>
            <button type="submit" class="btn btn-primary" > Buscar</button>
        </form>

        <!-- fim do fomulario de busca -->

        <!-- Itens do Menu Direita -->

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href= "" class="dropdown-toggle" data-toggle="dropdown">Cursos <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <?php foreach (obterCategoriasAtivas() as $categorias) : ?>
                   <li class="dropdown">
                    <a href="home.php?navegacao=<?= $categorias['desc_categoria']; ?>"><?= $categorias['desc_categoria']; ?></a>
                </li>

            <?php endforeach; ?>
        </ul>
    </li>



    <?php

    $obj_login = new Login();

    $verificacao_admin = $obj_login->verifica_admin_logado();

    if($verificacao_admin == false){
        echo '
        <li>
            <a href="home.php?navegacao=contato">Entre em Contato</a>
        </li>
        ';
    }

    ?>



    <?php

    if($verificacao_admin == true){
        $login_admin = $_SESSION['usuario']["login"];
        include_once 'conteudos_menu/admin.php';
    }

    $verificacao_logado = $obj_login->verifica_logado();

    if($verificacao_logado == true){
        $login = $_SESSION['usuario']["login"];
        include_once 'conteudos_menu/minha_conta.php';
    }else{
        include_once 'conteudos_menu/sem_conta.php';
    }

    ?>


</ul>

</div>
</div>
