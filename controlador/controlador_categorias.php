<?php

require_once 'Database.php';

function obterTodasCategorias()
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT cod_categoria, desc_categoria, status FROM categoria WHERE cod_categoria != 5 ");
    $categorias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $categorias;
}

function obterCategoriasAtivas()
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT cod_categoria, desc_categoria, status FROM categoria WHERE cod_categoria != 5 AND status = 1");
    $categorias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $categorias;
}

function obterDescCategoria($desc_categoria)
{
    if ( !isset($desc_categoria) ) {
        $desc_categoria = "Atualidades";
    }

    return $desc_categoria;
}

function obterCodCategoria($desc_categoria)
{
    $conn = Database::getConnection();

    $consulta = "SELECT cod_categoria FROM categoria WHERE desc_categoria = '$desc_categoria' ";
    $stmt = $conn->query($consulta);
    $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ( $resultado != null ) {
        $cod_categoria = $resultado[0]['cod_categoria'];
    } else {
        echo "<br>Conteúdo não encontrado! Veja a seguir as ATUALIDADES:";
        $cod_categoria = 0;
    }

    return $cod_categoria;
}

function contaNoticias($cod_categoria)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT count(id_noticia) FROM noticias WHERE cod_categoria = '$cod_categoria' ");
    $num_noticias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $num_noticias;
}

function obterDetalhesCategoria($cod_categoria)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT desc_categoria FROM categoria WHERE cod_categoria = '$cod_categoria' ");
    $detalhes = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $detalhes;
}
