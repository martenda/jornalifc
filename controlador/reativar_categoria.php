<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../visao/swal_alerts/dist/sweetalert.css">

<?php
require_once 'permite_acesso.php';
permiteAcessoAdmin();

$cod_categoria = filter_input(INPUT_GET, 'categoria');

require_once 'Database.php';
require_once 'valida_dados.php';

$cod_categoria = validaDados($cod_categoria, "Código da categoria");

if ($cod_categoria != false){

      $conn = Database::getConnection();

      $banco = "UPDATE categoria SET status = 1
                WHERE cod_categoria = '$cod_categoria'";
      $conn->exec($banco);

      echo("
            <script type='text/javascript'>
            location.href='../visao/home.php?navegacao=gerenciar_categorias';
            </script>");

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'A URL está incorreta! Tente novamente.',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../visao/home.php?navegacao=gerenciar_categorias';
            }
        );
        </script>");
}
