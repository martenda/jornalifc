<?php

class Database
{
    public static function getConnection()
    {
        $conn = null;
        try {
            $conn = new PDO ("mysql:host=localhost;dbname=jornal_ifc", 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Setando o banco para UTF-8
            $conn->query("SET NAMES 'utf8'");
            $conn->query('SET character_set_connection=utf8');
            $conn->query('SET character_set_client=utf8');
            $conn->query('SET character_set_results=utf8');

            return $conn;
        } catch (PDOException $erro) {
            echo "Conexão falhou: " . $erro->getMessage();
        }
    }
}
