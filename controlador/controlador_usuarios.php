<?php

require_once './../modelo/login.class.php';

function obterTodosUsuarios(){

    $conn = Database::getConnection();

    $usuarios = "SELECT id_usuario, nome, email, cpf, dt_nasc, id_tipo_usuario FROM usuario";
    $stmt = $conn->query($usuarios);
    $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $usuarios;
}
