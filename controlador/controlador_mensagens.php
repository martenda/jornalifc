<?php

require_once './../modelo/login.class.php';

function obterNomeContato($id_usuario)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT nome FROM usuario WHERE id_usuario = $id_usuario");
    $nome = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $nome[0]['nome'];
}

function obterEmailContato($id_usuario)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT email FROM usuario WHERE id_usuario = $id_usuario");
    $nome = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $nome[0]['email'];
}

function obterMensagensEnviadas(){

    $conn = Database::getConnection();

    $id_usuario = $_SESSION['usuario']['id'];

    $mensagens = "SELECT id_contato, tipo_contato, texto_contato, data_contato FROM contato WHERE id_usuario = '$id_usuario' ORDER BY id_contato DESC";
    $stmt = $conn->query($mensagens);
    $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $mensagens;
}

function obterMensagensRecebidas(){

    $conn = Database::getConnection();

    $mensagens = "SELECT id_contato, id_usuario, tipo_contato, texto_contato, data_contato FROM contato WHERE id_resposta = 0 ORDER BY id_contato DESC";
    $stmt = $conn->query($mensagens);
    $mensagens = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $mensagens;
}

function obterDetalhesContato($id_contato){

    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT id_contato, id_usuario, tipo_contato, texto_contato, data_contato, id_resposta FROM contato WHERE id_contato = '$id_contato' ");
    $detalhes = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $detalhes[0];
}

function obterResposta($id_contato)
{
    $conn = Database::getConnection();

    $consulta = $conn->query("SELECT id_contato, data_contato FROM contato WHERE id_resposta = '$id_contato' ");
    $resposta = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $resposta;
}
