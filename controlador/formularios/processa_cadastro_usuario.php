<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

require_once '../Database.php';
require_once '../valida_dados.php';
require_once '../../modelo/login.class.php';

$nome = validaDados($_POST['nome'], "Nome");
$email = validaDados($_POST['email'], "E-mail");
$cpf = validaDados($_POST['cpf'], "CPF");
$dt_nasc = validaDados($_POST['dt_nasc'], "Data de nascimento");
$senha = validaDados($_POST['senha'], "Senha");
$confirmar_senha = validaDados($_POST['confirmar_senha'], "Confirmar senha");

if ($nome != false && $email != false && $cpf != false && $dt_nasc != false && $senha != false){

    if ($senha == $confirmar_senha){

        $conn = Database::getConnection();

        // Verifica se já há um usuário cadastrado com este EMAIL
        $verificacao_email = "SELECT id_usuario FROM usuario WHERE email = '$email' ";
        $stmt = $conn->query($verificacao_email);
        $verificacao_email = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($verificacao_email == false){

            // Verifica se já há um usuário cadastrado com este CPF
            $verificacao_cpf = "SELECT id_usuario FROM usuario WHERE cpf = '$cpf' ";
            $stmt = $conn->query($verificacao_cpf);
            $verificacao_cpf = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($verificacao_cpf == false) {

                // Cadastra as informações do novo usuário no banco
                $banco = "INSERT INTO usuario (nome, email, cpf, dt_nasc, senha, id_tipo_usuario)
                          VALUES ('$nome', '$email', '$cpf', '$dt_nasc', '$senha', 2)";
                $conn->exec($banco);

                /*Faz o login*/
                $obj_login = new Login();
                $obj_login->fazer_login($email, $senha);

            }else {
                echo("<br>
                  <script type='text/javascript'>
                  swal(
                      {
                          title: 'Erro!',
                          text: 'CPF já cadastrado! Tente outro por favor :)',
                          type: 'error',
                          showCancelButton: false,
                          confirmButtonClass: 'btn-succes',
                          confirmButtonText: 'Ok'
                      },
                      function(){
                          location.href='../../visao/home.php?navegacao=cadastro_usuario';
                      }
                  );
                  </script>");
            }

        }else{
            echo("<br>
              <script type='text/javascript'>
              swal(
                  {
                      title: 'Erro!',
                      text: 'Já existe um usuário cadastrado com este e-mail! Tente outro por favor :)',
                      type: 'error',
                      showCancelButton: false,
                      confirmButtonClass: 'btn-succes',
                      confirmButtonText: 'Ok'
                  },
                  function(){
                      location.href='../../visao/home.php?navegacao=cadastro_usuario';
                  }
              );
              </script>");
        }

    }else{
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Sua senha não está igual à confirmação! Tente novamente por favor :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=cadastro_usuario';
              }
          );
          </script>");
    }

}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=cadastro_usuario';
          }
      );
      </script>");
}
