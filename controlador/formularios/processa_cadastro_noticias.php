<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

header('Content-Type: text/html; charset=utf-8');

session_start();

require_once '../Database.php';
require_once '../valida_dados.php';
require_once 'colocarMidia.php';
include_once '../ocultar_erros.php';

$titulo = validaDados($_POST['titulo'], "Título");
$texto = validaDados($_POST['texto'], "Texto");
  $texto = str_replace("\n",'<br />', $texto);
$tag = validaDados($_POST['tag'], "Tag de referência");
$categoria = (int) $_POST['categoria'];

if ($categoria == false) {
    $categoria = 0;
}

if ($titulo != false && $texto != false && $tag != false) {

    $conn = Database::getConnection();

    /*Verifica se já há uma notícia cadastrada com este título*/
    $verificacao = "SELECT id_noticia FROM noticias WHERE titulo_noticia = '$titulo' ";
    $stmt = $conn->query($verificacao);
    $verificacao = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($verificacao == false) {

        $id_usuario = $_SESSION['usuario']['id'];

        // Cadastra as informações da nova notícia no banco
        $banco = "INSERT INTO noticias (titulo_noticia, texto_noticia, tags_referencia, cod_categoria, id_usuario)
                  VALUES ('$titulo', '$texto', '$tag', $categoria, $id_usuario)";
        $conn->exec($banco);



        /******
        * Upload de imagens
        ******/

        $files = $_FILES['arquivos'];

        if ( $files['name'][0] != false ) {

            $consulta_idnoticia = "SELECT id_noticia FROM noticias WHERE titulo_noticia = '$titulo' ";
            $stmt = $conn->query($consulta_idnoticia);
            $resultado = $stmt->fetch(PDO::FETCH_ASSOC);

            $id_noticia = $resultado['id_noticia'];


            Midia($files, $id_noticia);
        }

        echo("<br>
              <script type='text/javascript'>
              swal(
                  {
                      title: 'Sucesso!',
                      text: 'Notícia cadastrada com sucesso! Vlw :)',
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonClass: 'btn-succes',
                      confirmButtonText: 'Ok'
                  },
                  function(){
                      location.href='../../visao/home.php';
                  }
              );
              </script>");

    }else{
        echo("<br>
            <script type='text/javascript'>
            swal(
                {
                    title: 'Erro!',
                    text: 'Já existe uma notícia cadastrada com este título! Tente novamente por favor :)',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonClass: 'btn-succes',
                    confirmButtonText: 'Ok'
                },
                function(){
                    location.href='../../visao/home.php?navegacao=cadastrar_noticias';
                }
            );
            </script>");
    }

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'Acreditamos que um campo foi preenchido com valor inválido... Por favor, verifique a notícia que você cadastrou e confira se não há erros. Caso haja algum, atualize-a! Obrigado :)',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../../visao/home.php';
            }
        );
        </script>");
}
