<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

session_start();

require_once '../Database.php';
require_once '../valida_dados.php';

$cod_cat = filter_input(INPUT_GET, 'categoria');

$desc = validaDados($_POST['desc'], "Descrição da categoria");

if ($desc != false) {

    $conn = Database::getConnection();

    //Verifica se já há uma categoria cadastrada com este título
    $verificacao = "SELECT cod_categoria FROM categoria WHERE cod_categoria = '$cod_cat' ";
    $stmt = $conn->query($verificacao);
    $verificacao = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($verificacao == false || $verificacao['cod_categoria'] == $cod_cat) {

        // Atualiza as informações da notícia no banco
        $banco = "UPDATE categoria SET desc_categoria = '$desc' WHERE cod_categoria = '$cod_cat'";
        $conn->exec($banco);

        echo("<br>
              <script type='text/javascript'>
                swal(
                    {
                        title: 'Sucesso!',
                        text: 'Categoria atualizada com sucesso! Vlw :)',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(){
                        location.href='../../visao/home.php?navegacao=gerenciar_categorias';
                    }
                );
              </script>");

    } else {
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Já existe uma categoria cadastrada com este título! Tente outro título por favor :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=atualizar_categoria&categoria=$cod_cat';
              }
          );
          </script>");
    }


}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=atualizar_categoria&categoria=$cod_cat';
          }
      );
      </script>");
}
