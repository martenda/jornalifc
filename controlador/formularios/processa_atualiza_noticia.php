<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

session_start();

require_once '../Database.php';
require_once '../valida_dados.php';
require_once 'colocarMidia.php';
include_once '../ocultar_erros.php';

$id_noticia = filter_input(INPUT_GET, 'noticia');

$titulo = validaDados($_POST['titulo'], "Título");
$texto = validaDados($_POST['texto'], "Texto");
  $texto = str_replace("\n",'<br />', $texto);
$tag = validaDados($_POST['tag'], "Tag de referência");
$categoria = (int) $_POST['categoria'];

if ($categoria == false) {
    $categoria = 0;
}

if ($titulo != false && $texto != false && $tag != false) {

    $conn = Database::getConnection();

    /*Verifica se já há uma notícia cadastrada com este título*/
    $verificacao = "SELECT id_noticia FROM noticias WHERE titulo_noticia = '$titulo' ";
    $stmt = $conn->query($verificacao);
    $verificacao = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($verificacao == false || $verificacao['id_noticia'] == $id_noticia) {

        // Atualiza as informações da notícia no banco
        $banco = "UPDATE noticias SET titulo_noticia = '$titulo', texto_noticia = '$texto', tags_referencia = '$tag', cod_categoria = $categoria
        WHERE id_noticia = '$id_noticia'";
        $conn->exec($banco);



        /******
        * Upload de imagens
        ******/

        $files = $_FILES['arquivos'];

        if ( $files['name'][0] != false ) {
            Midia($files, $id_noticia);
        }

        echo("<br>
              <script type='text/javascript'>
                swal(
                    {
                        title: 'Sucesso!',
                        text: 'Notícia atualizada com sucesso! Vlw :)',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(){
                        location.href='../../visao/home.php?navegacao=ler_noticia&noticia=$titulo';
                    }
                );
              </script>");

    } else {
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Já existe uma notícia cadastrada com este título! Tente outro título por favor :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=atualizar_noticia&noticia=$id_noticia';
              }
          );
          </script>");
    }


}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Acreditamos que um campo foi preenchido com valor inválido... Por favor, verifique esta notícia e confira se não há erros. Caso haja algum, atualize-a agora! Obrigado :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=atualizar_noticia&noticia=$id_noticia';
          }
      );
      </script>");
}
