<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">


<?php

session_start();

require_once '../Database.php';
require_once '../valida_dados.php';

$nome = validaDados($_POST['nome'], "Nome");
$email = validaDados($_POST['email'], "E-mail");
$dt_nasc = validaDados($_POST['dt_nasc'], "Data de nascimento");

if ($nome != false && $email != false && $dt_nasc != false) {

    $conn = Database::getConnection();

    // Verifica se já há um usuário cadastrado com este email
    $verificacao = "SELECT id_usuario FROM usuario WHERE email = '$email' ";
    $stmt = $conn->query($verificacao);
    $verificacao = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($verificacao == false || $verificacao['id_usuario'] == $_SESSION['usuario']['id']) {

        // Atualiza as informações do usuário já existente no banco

        $id_usuario = $_SESSION['usuario']['id'];

        $banco = "UPDATE usuario SET nome = '$nome', email = '$email', dt_nasc = '$dt_nasc'
        WHERE id_usuario = '$id_usuario'";
        $conn->exec($banco);

        $_SESSION['usuario']["login"] = $email;

          echo("<br>
              <script type='text/javascript'>
              swal(
                  {
                      title: 'Sucesso!',
                      text: 'Seus dados foram atualizados com sucesso! Vlw :)',
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonClass: 'btn-succes',
                      confirmButtonText: 'Ok'
                  },
                  function(){
                      location.href='../../visao/home.php?navegacao=detalhes_da_conta';
                  }
              );
              </script>");

    } else {
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Já existe um usuário cadastrado com este e-mail! Tente outro por favor :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=atualizar_informacoes';
              }
          );
          </script>");
    }


}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você deixou campos vazios ou os preencheu com valores inválidos! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=atualizar_informacoes';
          }
      );
      </script>");
}
