<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

include_once '../ocultar_erros.php';

function Midia($files, $id_noticia){
	if(isset($files)){

		date_default_timezone_set("America/Sao_Paulo");
		$formatos = array("jpg", "jpeg", "png", "gif", "bmp");
		$up_dir = '../../visao/imgs/';
		$dir = "visao/imgs/";
		$conn = Database::getConnection();
		$tamanho_max = 2000000;

		// Tirar o "=> $name"
		foreach ($files['name'] as $file => $name) {
				$nome = $files['name'][$file];

				if ($files['error'][$file] != 0) {
              echo("<br>
                  <script type='text/javascript'>
                  swal(
                      {
                          title: 'Erro ao cadastrar mídia!',
                          text: 'Desculpe, mas o arquivo $name não pode ser salvo em nosso sistema, ele contém um erro. Tente outro arquivo por favor :)',
                          type: 'error',
                          showCancelButton: false,
                          confirmButtonClass: 'btn-succes',
                          confirmButtonText: 'Ok'
                      },
                      function(){
                          location.href='';
                      }
                  );
                  </script>");
							exit;
			    } else {
			    		if ($files['size'][$file] > $tamanho_max) {
                  echo("<br>
                      <script type='text/javascript'>
                      swal(
                          {
                              title: 'Erro ao cadastrar mídia!',
                              text: '$name é um arquivo muito grande ({$files['size'][$file]}B), o tamanho máximo permitido é de {$tamanho_max}B. Tente outro arquivo por favor :)',
                              type: 'error',
                              showCancelButton: false,
                              confirmButtonClass: 'btn-succes',
                              confirmButtonText: 'Ok'
                          },
                          function(){
                              location.href='';
                          }
                      );
                      </script>");
									exit;
			        } elseif(!in_array(pathinfo($name, PATHINFO_EXTENSION), $formatos)){
                  echo("<br>
                      <script type='text/javascript'>
                      swal(
                          {
                              title: 'Erro ao cadastrar mídia!',
                              text: '$name nao é um formato válido! Tente outro arquivo por favor :)',
                              type: 'error',
                              showCancelButton: false,
                              confirmButtonClass: 'btn-succes',
                              confirmButtonText: 'Ok'
                          },
                          function(){
                              location.href='';
                          }
                      );
                      </script>");
									exit;
							} else {
									// move arquivo para a pasta desejada
									$caminho = $up_dir.$nome;
									move_uploaded_file($files["tmp_name"][$file], $caminho);

									// insere a mídia no banco
									$caminho = $dir.$nome;
									$tipo = $files['type'][$file];

					        $banco = "INSERT INTO midia (caminho, titulo, tipo, id_noticia)
														VALUES ('$caminho', '$nome', '$tipo', '$id_noticia')";
							    $conn->exec($banco);
			        }
			    }
			}
	}
}
