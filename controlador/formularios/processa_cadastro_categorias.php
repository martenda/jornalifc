<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

header('Content-Type: text/html; charset=utf-8');

require_once '../Database.php';
require_once '../valida_dados.php';

$titulo = validaDados($_POST['titulo'], "Título da categoria");

if ($titulo != false){

    $conn = Database::getConnection();

    // Verifica se já há uma categoria cadastrada com este título
    $verificacao = "SELECT cod_categoria FROM categoria WHERE desc_categoria = '$titulo' ";
    $stmt = $conn->query($verificacao);
    $verificacao = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($verificacao == false)
    {
        // Cadastra as informações da nova categoria no banco
        $banco = "INSERT INTO categoria (desc_categoria, status)
                  VALUES ('$titulo', 1)";
        $conn->exec($banco);

        echo("<br>
              <script type='text/javascript'>
                swal(
                    {
                        title: 'Sucesso!',
                        text: 'Categoria cadastrada com sucesso! Vlw :)',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(){
                        location.href='../../visao/home.php';
                    }
                );
              </script>");

    }else{
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Já existe uma categoria cadastrada com este título! Tente novamente por favor :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=cadastrar_categorias';
              }
          );
          </script>");
    }

}else{
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../../visao/home.php?navegacao=cadastrar_categorias';
            }
        );
        </script>");
}
