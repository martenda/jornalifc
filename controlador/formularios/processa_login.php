<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

require_once '../valida_dados.php';
require_once '../Database.php';
require_once '../../modelo/login.class.php';

$email = validaDados($_POST['email'], "E-mail");
$senha = validaDados($_POST['senha'], "Senha");

if ($email != false && $senha != false){

    $obj_login = new Login();
    $obj_login->fazer_login($email, $senha);

}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=login';
          }
      );
      </script>");
}
