<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

session_start();

require_once '../Database.php';
require_once '../valida_dados.php';

$senha_atual = validaDados($_POST['senha_atual'], "Senha atual");
$nova_senha = validaDados($_POST['nova_senha'], "Nova senha");
$confirmar_senha = validaDados($_POST['confirmar_senha'], "Confirmar senha");

if ($senha_atual != false && $nova_senha != false && $confirmar_senha != false){

      $conn = Database::getConnection();

      $id_usuario = $_SESSION['usuario']['id'];

      $verificacao = "SELECT senha FROM usuario WHERE id_usuario = '$id_usuario' ";
      $stmt = $conn->query($verificacao);
      $senha_original = $stmt->fetch(PDO::FETCH_ASSOC);

      if ($senha_atual == $senha_original['senha']) {

          if ($nova_senha == $confirmar_senha) {

              $banco = "UPDATE usuario SET senha = '$nova_senha'
                        WHERE id_usuario = '$id_usuario'";
              $conn->exec($banco);

              echo("<br>
                    <script type='text/javascript'>
                      swal(
                          {
                              title: 'Sucesso!',
                              text: 'A senha foi atualizada! Vlw :)',
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonClass: 'btn-succes',
                              confirmButtonText: 'Ok',
                              closeOnConfirm: false,
                              closeOnCancel: true
                          },
                          function(){
                              location.href='../../visao/home.php?navegacao=detalhes_da_conta';
                          }
                      );
                    </script>");

          }else{
              echo("<br>
                <script type='text/javascript'>
                swal(
                    {
                        title: 'Erro!',
                        text: 'A nova senha não está igual à confirmação! Tente novamente por favor :)',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok'
                    },
                    function(){
                        location.href='../../visao/home.php?navegacao=alterar_senha';
                    }
                );
                </script>");
          }

      }else{
          echo("<br>
            <script type='text/javascript'>
            swal(
                {
                    title: 'Erro!',
                    text: 'Senha atual incorreta! Digite a sua senha atual corretamente por favor.',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonClass: 'btn-succes',
                    confirmButtonText: 'Ok'
                },
                function(){
                    location.href='../../visao/home.php?navegacao=alterar_senha';
                }
            );
            </script>");
      }

}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=alterar_senha';
          }
      );
      </script>");
}
