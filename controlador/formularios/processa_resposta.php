<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

if(!isset($_SESSION))
{
    session_start();
}

require_once '../Database.php';
require '../valida_dados.php';

$id_contato = validaDados($_POST['id_contato'], "ID do contato");
$tipo_resposta = validaDados($_POST['tipo_resposta'], "Tipo da resposta");
$resposta = validaDados($_POST['resposta'], "Resposta");

if ($id_contato != false && $tipo_resposta != false && $resposta != false){

    $conn = Database::getConnection();

        date_default_timezone_set('America/Sao_Paulo');
        $dt_atual = date('Y-m-d');

        $id_usuario = $_SESSION['usuario']['id'];

        // Cadastra a resposta no banco
        $banco = "INSERT INTO contato (tipo_contato, texto_contato, data_contato, id_usuario, id_resposta)
        VALUES ('$tipo_resposta', '$resposta', '$dt_atual', $id_usuario, $id_contato)";
        $conn->exec($banco);

        echo("<br>
              <script type='text/javascript'>
                swal(
                    {
                        title: 'Sucesso!',
                        text: 'A mensagem foi respondida com sucesso! Vlw :)',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(){
                        location.href='../../visao/home.php?navegacao=ver_mensagens';
                    }
                );
              </script>");

}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=responder_mensagem&contato=$id_contato';
          }
      );
      </script>");
}
