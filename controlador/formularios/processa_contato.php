<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

if(!isset($_SESSION))
{
    session_start();
}

require_once '../Database.php';
require '../valida_dados.php';

$motivo_contato = validaDados($_POST['motivo_contato'], "Motivo do contato");
$mensagem = validaDados($_POST['mensagem'], "Mensagem");

if ($motivo_contato != false && $mensagem != false){

    $conn = Database::getConnection();

    // Verifica se o usuário está logado
    if ( isset($_SESSION['usuario']) && $_SESSION['usuario']['esta_logado'] == true){

        date_default_timezone_set('America/Sao_Paulo');
        $dt_atual = date('Y-m-d');

        $id_usuario = $_SESSION['usuario']['id'];

        // Cadastra o contato no banco
        $banco = "INSERT INTO contato (tipo_contato, texto_contato, data_contato, id_usuario, id_resposta)
        VALUES ('$motivo_contato', '$mensagem', '$dt_atual', $id_usuario, 0)";
        $conn->exec($banco);

        echo("<br>
              <script type='text/javascript'>
                swal(
                    {
                        title: 'Sucesso!',
                        text: 'Sua mensagem foi enviada com sucesso! Vlw :)',
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(){
                        location.href='../../visao/home.php?navegacao=mensagens_enviadas';
                    }
                );
              </script>");

    }else{
        echo("<br>
          <script type='text/javascript'>
          swal(
              {
                  title: 'Erro!',
                  text: 'Você não está logado! Por favor, efetue o login antes de tentar nos contatar :)',
                  type: 'error',
                  showCancelButton: false,
                  confirmButtonClass: 'btn-succes',
                  confirmButtonText: 'Ok'
              },
              function(){
                  location.href='../../visao/home.php?navegacao=login';
              }
          );
          </script>");
    }

}else{
    echo("<br>
      <script type='text/javascript'>
      swal(
          {
              title: 'Erro!',
              text: 'Você preencheu um campo com um valor inválido! Tente novamente por favor :)',
              type: 'error',
              showCancelButton: false,
              confirmButtonClass: 'btn-succes',
              confirmButtonText: 'Ok'
          },
          function(){
              location.href='../../visao/home.php?navegacao=contato';
          }
      );
      </script>");
}
