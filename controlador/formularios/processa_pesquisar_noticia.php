<!-- Swal alert style -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

<!-- Swal alert -->
<script src="../../visao/swal_alerts/dist/sweetalert.js"></script>
<link rel="stylesheet" href="../../visao/swal_alerts/dist/sweetalert.css">

<?php

require_once '../Database.php';
require '../valida_dados.php';

$pesquisa = validaDados($_GET['pesquisa'], "Pesquisa");

if ($pesquisa != false) {

    if (strlen($pesquisa) >= 3){

        $conn = Database::getConnection();

        //pesquisa a notícia pelo título e pela tag
        $banco = "SELECT id_noticia FROM noticias WHERE titulo_noticia LIKE '%$pesquisa%' OR tags_referencia LIKE '%$pesquisa%' ";
        $stmt = $conn->query($banco);
        $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado != false) {
            echo("
                <script type='text/javascript'>
                location.href='../../visao/home.php?navegacao=pesquisas&noticias={$resultado[0]['id_noticia']}';
                </script>");
        }else {
            echo("<br>
                <script type='text/javascript'>
                swal(
                    {
                        title: 'Erro!',
                        text: 'Desculpe, mas não encontramos nada... Tente pesquisar novamente por favor :)',
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonClass: 'btn-succes',
                        confirmButtonText: 'Ok'
                    },
                    function(){
                        location.href='../../visao/home.php';
                    }
                );
                </script>");
        }

    }else{
        echo("<br>
            <script type='text/javascript'>
            swal(
                {
                    title: 'Erro!',
                    text: 'Sua pesquisa deve ter mais de 2 dígitos... Tente novamente por favor :)',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonClass: 'btn-succes',
                    confirmButtonText: 'Ok'
                },
                function(){
                    location.href='../../visao/home.php';
                }
            );
            </script>");
    }

} else {
    echo("<br>
        <script type='text/javascript'>
        swal(
            {
                title: 'Erro!',
                text: 'Você preencheu o campo de pesquisa com um valor inválido! Tente novamente por favor :)',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-succes',
                confirmButtonText: 'Ok'
            },
            function(){
                location.href='../../visao/home.php';
            }
        );
        </script>");
}
