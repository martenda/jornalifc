-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23-Nov-2017 às 03:07
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jornal_ifc`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `obterAutorNoticia` (`id_user` INT(11)) RETURNS VARCHAR(80) CHARSET utf8 BEGIN
declare nome_user varchar(80);
select nome into nome_user
from usuario
where id_usuario = id_user;
RETURN nome_user;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `cod_categoria` int(11) NOT NULL,
  `desc_categoria` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`cod_categoria`, `desc_categoria`, `status`) VALUES
(0, 'Atualidades', 1),
(1, 'Agropecuária', 1),
(2, 'Informática', 1),
(3, 'Química', 1),
(4, 'Outras Notícias', 1),
(5, 'Notícias Desativadas', 1),
(6, 'tst categoria', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id_contato` int(11) NOT NULL,
  `tipo_contato` varchar(15) DEFAULT NULL,
  `texto_contato` varchar(1000) DEFAULT NULL,
  `data_contato` date DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_resposta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id_contato`, `tipo_contato`, `texto_contato`, `data_contato`, `id_usuario`, `id_resposta`) VALUES
(1, 'Dúvida', 'Dúvida exemplo qualquer 1', '2016-11-29', 5, 0),
(2, 'Sugestão', 'Sugestão exemplo qualquer 1', '2016-11-29', 5, 0),
(3, 'Reclamação', 'Reclamação exemplo qualquer 1', '2016-11-30', 5, 0),
(4, 'Reclamação', 'Reclamação exemplo qualquer 2', '2016-12-05', 7, 0),
(5, 'Dúvida', 'Dúvida exemplo qualquer 2', '2016-12-09', 7, 0),
(6, 'Sugestão', 'Sugestão exemplo qualquer 2', '2016-12-09', 4, 0),
(7, 'Dúvida', 'Dúvida exemplo qualquer 3', '2016-12-15', 5, 0),
(8, 'Dúvida', 'Dúvida exemplo qualquer 4', '2016-12-15', 5, 0),
(9, 'Sugestão', 'Sugestão exemplo qualquer 3', '2016-12-16', 4, 0),
(10, 'Esclarecimento ', 'Esclarecimento de dúvida exemplo qualquer 1', '2017-09-21', 6, 1),
(11, 'Agradecimento d', 'Agradecimento de sugestão qualquer exemplo 1', '2017-10-05', 6, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE `midia` (
  `cod_midia` int(11) NOT NULL,
  `caminho` varchar(300) DEFAULT NULL,
  `titulo` varchar(80) DEFAULT NULL,
  `tipo` varchar(80) DEFAULT NULL,
  `id_noticia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`cod_midia`, `caminho`, `titulo`, `tipo`, `id_noticia`) VALUES
(27, 'visao/imgs/processo_seletivo.png', 'processo_seletivo.png', 'image/png', 35),
(61, 'visao/imgs/pi5.jpg', 'pi5.jpg', 'image/jpeg', 56),
(62, 'visao/imgs/pi6.jpg', 'pi6.jpg', 'image/jpeg', 56),
(64, 'visao/imgs/pi4.jpg', 'pi4.jpg', 'image/jpeg', 56),
(65, 'visao/imgs/tesouro1.jpg', 'tesouro1.jpg', 'image/jpeg', 58),
(66, 'visao/imgs/tesouro2.jpg', 'tesouro2.jpg', 'image/jpeg', 58),
(67, 'visao/imgs/tesouro3.jpg', 'tesouro3.jpg', 'image/jpeg', 58),
(68, 'visao/imgs/tesouro4.jpg', 'tesouro4.jpg', 'image/jpeg', 58),
(69, 'visao/imgs/tesouro5.jpg', 'tesouro5.jpg', 'image/jpeg', 58),
(70, 'visao/imgs/tesouro6.jpg', 'tesouro6.jpg', 'image/jpeg', 58),
(77, 'visao/imgs/oficina_jornalismo1.jpg', 'oficina_jornalismo1.jpg', 'image/jpeg', 59),
(78, 'visao/imgs/oficina_jornalismo2.jpg', 'oficina_jornalismo2.jpg', 'image/jpeg', 59),
(79, 'visao/imgs/micti1.jpg', 'micti1.jpg', 'image/jpeg', 60),
(80, 'visao/imgs/micti8.jpg', 'micti8.jpg', 'image/jpeg', 60),
(82, 'visao/imgs/almosom4.jpg', 'almosom4.jpg', 'image/jpeg', 61),
(83, 'visao/imgs/almosom2.jpg', 'almosom2.jpg', 'image/jpeg', 61),
(84, 'visao/imgs/almosom3.jpg', 'almosom3.jpg', 'image/jpeg', 61),
(91, 'visao/imgs/sabao.jpg', 'sabao.jpg', 'image/jpeg', 76),
(92, 'visao/imgs/periodica.jpg', 'periodica.jpg', 'image/jpeg', 74),
(93, 'visao/imgs/molecula.jpg', 'molecula.jpg', 'image/jpeg', 75),
(94, 'visao/imgs/pib.jpg', 'pib.jpg', 'image/jpeg', 79),
(95, 'visao/imgs/boi.jpg', 'boi.jpg', 'image/jpeg', 78),
(96, 'visao/imgs/boi2.jpg', 'boi2.jpg', 'image/jpeg', 78),
(97, 'visao/imgs/RenovaBio.jpg', 'RenovaBio.jpg', 'image/jpeg', 80),
(99, 'visao/imgs/cebola2.jpg', 'cebola2.jpg', 'image/jpeg', 81),
(100, 'visao/imgs/cebola.jpg', 'cebola.jpg', 'image/jpeg', 81),
(102, 'visao/imgs/le1.jpg', 'le1.jpg', 'image/jpeg', 77),
(103, 'visao/imgs/leite-low-carb-coco.jpg', 'leite-low-carb-coco.jpg', 'image/jpeg', 77),
(104, 'visao/imgs/download.jpg', 'download.jpg', 'image/jpeg', 77),
(105, 'visao/imgs/virus.jpg', 'virus.jpg', 'image/jpeg', 65),
(107, 'visao/imgs/dell1.jpg', 'dell1.jpg', 'image/jpeg', 62),
(108, 'visao/imgs/screen-shot-2017-11-19-at-5.30.27-pm.png', 'screen-shot-2017-11-19-at-5.30.27-pm.png', 'image/png', 63),
(110, 'visao/imgs/IMG_2321-150x150.png', 'IMG_2321-150x150.png', 'image/png', 68),
(111, 'visao/imgs/IMG_2332-2-150x150.jpg', 'IMG_2332-2-150x150.jpg', 'image/jpeg', 68),
(112, 'visao/imgs/WhatsApp-Image-2017-07-05-at-12.51.24-150x150.jpeg', 'WhatsApp-Image-2017-07-05-at-12.51.24-150x150.jpeg', 'image/jpeg', 68),
(114, 'visao/imgs/piexemplo.jpg', 'piexemplo.jpg', 'image/jpeg', 73),
(115, 'visao/imgs/08-150x150.jpeg', '08-150x150.jpeg', 'image/jpeg', 72),
(116, 'visao/imgs/09-150x150.jpeg', '09-150x150.jpeg', 'image/jpeg', 72),
(117, 'visao/imgs/12-150x150.jpeg', '12-150x150.jpeg', 'image/jpeg', 72),
(118, 'visao/imgs/27-150x150.jpg', '27-150x150.jpg', 'image/jpeg', 72),
(120, 'visao/imgs/cliffhouse-800x450.jpg', 'cliffhouse-800x450.jpg', 'image/jpeg', 66),
(121, 'visao/imgs/mw-860.jpg', 'mw-860.jpg', 'image/jpeg', 64),
(122, 'visao/imgs/06-150x150.jpeg', '06-150x150.jpeg', 'image/jpeg', 72),
(123, 'visao/imgs/FOTO-SITE-IFC-1-300x225.jpg', 'FOTO-SITE-IFC-1-300x225.jpg', 'image/jpeg', 70),
(124, 'visao/imgs/reu.jpg', 'reu.jpg', 'image/jpeg', 69),
(125, 'visao/imgs/ii.jpg', 'ii.jpg', 'image/jpeg', 71),
(126, 'visao/imgs/3.png', '3.png', 'image/png', 66);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `titulo_noticia` varchar(200) DEFAULT NULL,
  `texto_noticia` varchar(5000) DEFAULT NULL,
  `tags_referencia` varchar(250) DEFAULT NULL,
  `cod_categoria` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `titulo_noticia`, `texto_noticia`, `tags_referencia`, `cod_categoria`, `id_usuario`) VALUES
(35, 'IFC oferta 2.075 vagas gratuitas em cursos técnicos integrados', '[21/07/2017]   O Instituto Federal Catarinense (IFC) é para todos. A partir de segunda-feira (24/07) iniciam as inscrições para o Exame de Classificação 2018 aos candidatos que desejam fazer o curso técnico integrado em ensino médio gratuito em uma instituição federal. São ofertadas 2.075 vagas para candidatos que tenham cursado e concluído todo o ensino fundamental [...]', 'Processo Seletivo', 0, 1),
(36, 'Mais de 200 vagas para cursos gratuitos de qualificação profissional no IFC Araquari', '[19/07/2017]   O Instituto Federal Catarinense Campus Araquari está com editais abertos para cursos de qualificação profissional, são três diferentes possibilidades de escolha para aqueles que estão buscando aperfeiçoamento e/ou novos conhecimentos. Todos os cursos são abertos à participação da população, ofertados gratuitamente e, em geral, não relacionados ao nível de escolaridade. Cada curso possui características [...]', 'Cursos gratuitos', 0, 1),
(38, 'Tst status', 'tst status', 't', 5, 6),
(56, 'Curiosidades do Painel de Integração, você viu?', 'O Painel de Integração só ocorre, neste formato, no Campus Araquari, em todo o Instituto Federal Catarinense,  com o intuito de preparar os alunos do Ensino Médio para os trabalhos científicos do Ensino Superior.\r<br />\r<br />Esse é o primeiro ano em que o Painel aconteceu também no sábado. A pedido dos pais o evento ganhou mais um dia e passou a ocorrer também durante o dia 16, por ser mais acessível para os pais dos alunos e para as visitas em geral.\r<br />\r<br />O evento contou com mais de 100 apresentações, com os mais diversos temas e importantes questões sociais, que estimularam o conhecimento e o debate de ideias.\r<br />\r<br />Um fator que chamou atenção foi o aluno que se fantasiou de Hitler, para apresentar um projeto sobre as mudanças sociais a partir das guerras no mundo. O estudante foi notado por onde passou.\r<br />\r<br />Outro projeto que despertou a curiosidade de todos foi o Pomar, que plantou árvores de tangerina, poncã, e etc. O objetivo do projeto era fazer um local que poderia ser utilizado pelos alunos para recreação e as frutas comidas pelo mesmo.\r<br />\r<br />O custo para decoração dos stands foi relativamente baixo, girou em torno de 30 a 90 reais, com algumas exceções. Alguns stands também contaram com patrocínio e tiveram o custo zero, com a colaboração dos participantes fazendo um projeto comunitário para arrecadar os materiais necessários para o stand.\r<br />\r<br />O tempo para a elaboração do projeto foi em média de 2 meses, alguns projetos foram terminados nos últimos dias do prazo e outros foram elaborados por mais tempo, alguns alunos estavam com a ideia do projeto desde o ano passado.\r<br />\r<br />Os professores se surpreenderam com a participação alta na abertura da SEPE feita na sexta-feira, 15 de outubro, durante o Painel de Integração. O Auditório estava lotado e os alunos ouviram os discursos de Jonas, Cleder, Grasiela e Erica, representando as direções e coordenações da Instituição.', 'Painel de Integração', 0, 6),
(57, 'testetag01', 'testetag01', 'tagtst', 5, 6),
(58, 'Caça ao Tesouro no Campus Araquari', 'Nos dias 6, 7 e 10 de novembro realizamos com as turmas 1info1, 1info2 e 1info3 no nosso campus o primeiro evento “Caça ao Tesouro”, uma parceria entre as disciplinas de Artes e Geografia. Um jogo colaborativo foi criado, no qual os professores elaboraram instruções cartográficas e desafios artísticos a partir de conteúdos previstos para as disciplinas e mapas do campus elaborados pelos próprios alunos em oficinas. Além dos “caçadores”, cada equipe foi composta por um jogador do corpo técnico-cartográfico, que a partir de sua base, repassava aos colegas as instruções adquiridas a partir de sistema de informações geográficas, proporcionando a todos um processo de ensino-aprendizagem interativo e interdisciplinar.\r<br />\r<br />O objetivo a partir desta primeira experiência é utilizar o jogo com os calouros do ano seguinte, tendo os jogadores deste ano como “mentores”, criando assim um ambiente de maior interação entre os estudantes e proporcionando um maior conhecimento do campus por parte dos integrantes.', 'Gincanas', 0, 1),
(59, 'Oficina gratuita de fotojornalismo e vídeo acontecerá no IFC Araquari', 'Para dar continuidade à proposta de criação de um jornal comunitário dentro do IFC Araquari, na próxima quinta-feira (16) será oferecida uma oficina sobre noções básicas de fotojornalismo e vídeo, a partir das 14h. A atividade, que acontecerá no Bloco B – Sala B4, é gratuita e aberta para todos os interessados, sem pré-requisitos para participação.\r<br />\r<br />A ideia de um jornal comunitário na instituição surgiu de uma demanda dos próprios alunos e vem sendo amadurecida desde o movimento estudantil que ocorreu em outubro de 2016. Várias reuniões e oficinas tem acontecido para dar vida a este projeto. O IFC Araquari conta com a parceria do curso de Jornalismo do Bom Jesus Ielusc, e tem oferecido a possibilidade de a comunidade participar de treinamentos sobre diversas áreas do jornalismo, com temáticas como os critérios de noticiabilidade, formato de um texto jornalístico, como entrevistar e organizar uma notícia.\r<br />\r<br />Para a próxima quinta-feira o assunto abordado será fotografia e vídeo, com o foco jornalístico. A ideia é que até o final deste ano haja uma estrutura e conhecimentos mínimos para o início de um Jornal Comunitário dentro do IFC Araquari, escrito por estudantes e servidores, com assuntos de interesse de todos. O jornal será veiculado de modo digital, através de um aplicativo criado pela Fábrica de Software.', 'Oficina', 0, 1),
(60, 'Estudantes de Araquari conquistam ótimos resultados na X MICTI', 'A abertura da décima Mostra Nacional de Iniciação Científica e Tecnológica Interdisciplinar (Micti) e do terceiro IFCultura aconteceu na tarde do dia 8 de novembro, no Campus Camboriú do Instituto Federal Catarinense (IFC). Os eventos, que têm sido realizados de maneira concomitante há três anos, seguem com a programação até hoje (9). A simultaneidade reflete o caráter interdisciplinar e integrador do conhecimento: a proposta é mesclar pesquisa, extensão e ensino com atividades artístico-culturais.\r<br />\r<br />A Micti é um evento científico de exposição multidisciplinar, promovido e coordenado pelo IFC, que visa proporcionar a divulgação de trabalhos de pesquisa e de extensão desenvolvidos no próprio Instituto e em outras instituições de ensino do Brasil. Há três anos, o IFCultura vem sendo incorporado à Micti. A opção pela coexistência tem sido exitosa, na medida em que a exposição conjunta da pesquisa, da extensão e das manifestações artísticas e culturais possibilitam um olhar mais sensível ao desenvolvimento social e educacional.\r<br />\r<br />Com a maior delegação do evento, o IFC Araquari levou 39 trabalhos científicos para apresentação nas diferentes modalidades da MICTI, mais 16 estudantes que concorreram com apresentações artísticas no IFCultura. Para o IFCultura o Grupo de Dança Gaúcha Alma de Galpão dançou uma música típica do Rio Grande do Sul, e representando também os artistas de Araquari o Trio Zamba, composto pelos estudantes André Gustavo Fucker, Sesóstris Tomás Zamba e Renan Ângelo da Silva, empolgou os ouvintes com a sua apresentação.\r<br />\r<br />Como resultado das apresentações científicas, foram X premiados do Campus Araquari, sendo eles:\r<br />\r<br />3º LUGAR – Ciências Biológicas: Hidrolato de Curcuma Longa como redutor de estresse no transporte de tilápia do Nilo. (Amanda Chaaban, Julia Hess, Daniéli Broilo, Adolfo Jatobá, Daiane Oliveira, Marina Pereira);\r<br />3º LUGAR – Multidisciplinar: Levantamento documental das ações de pesquisa desenvolvidas no IFC Campus Araquari. (Katia Hardt, Cristiane Correa, Bruna Alves, Eduardo Silva);\r<br />3º LUGAR – Ciências Exatas e da Terra: Preparação de derivador triazólicos a partir de óleo essencial. (Ingrisson Santos, Poliana Gazolla, Eliakin Borba, Róbson Teixeira, Adalberto Silva);\r<br />2º LUGAR – Ciências Biológicas: Viabilidade do lactobacillus plantarum no manejo alimentar de tilápia do nilo. (Laura Silva, Andressa Moraes, Marina Pereira, Adolfo Jatobá);\r<br />1º LUGAR – Saúde: Zoonoses e saúde pública: abordagem lúdica com crianças do ensino fundamental no contexto da saúde única. (João Florêncio, Thiago Pereira, Camila Nogueira, Nícolas Felgueiras Belini Claro, Deolinda Carneiro);\r<br />1º LUGAR – Ciências Biológicas: Estudo da dimetilacetamida e erilenoglicol na criopreservação de sêmen suíno. (Ivan Bianchi, Fabiana Moreira, Thomaz Lucia Junior, Rafael Mondadori, Arnaldo Vieira, Mariah Schuch Monke Quirino, Kebb Borstnez);\r<br />1º LUGAR – Educação: Cavalaria de ideias: apresentando sobre cavalos, médicos veterinários e policiais militares montados. (Erika Mattos, Ana Cláudia Flores, Jéssica Aline Alves, Bethânia da Rocha Medeiros, Thaís Regina Lemfers);\r<br />1º LUGAR – Ciências Exatas e da Terra : Desenvolvimento de uma aplicação web para catalogação de plantas da região norte de Santa Catarina: herbário online. (Fernanda Cidade, Gabriel Silveira, Fábio de Moura, Ana Paula Camargo, Christopher Theilacher);\r<br />1º LUGAR – Meio Ambiente: Recicla (Grazielly Vilmes, Anelise Destefani, Ana Julia Soberanski Gorski, Nathaly Eloise Henning, Thaynara Cichoski);\r<br />1º LUGAR – Cultura: A vivência de um coletivo de artes na perspectiva de uma bolsista de extensão. (Alessandra Klug, Ellen D’Aguiar);\r<br />1º LUGAR – Educação: Ciclo de palestras sobre segurança do trabalho. (Bruno Carlesso Aita, Leandro Kingeski Pachco, Monique Ersching, Leandro Mondini, Eduardo Lopes Oliveira, William Estevão Araúoj, Jessiva Hipolito SAldanha, Sidiane Maciel de Souza Marinho).', 'MICTI', 0, 1),
(61, 'Almosom especial acontecerá no próximo dia 30', 'No próximo dia 14 de novembro acontecerá mais uma edição do Almosom! Para este mês foi convidado o Grupo de Metais da Associação do Corpo de Bombeiros Voluntários de Joinville, que fará uma apresentação especial no IFC Araquari, a partir das 12h, no espaço ao lado da Cantina do Bloco E.\r<br />\r<br />A fim de proporcionar alguns momentos diferentes de cultura e lazer para os estudantes, a proposta neste mês é trazer um tipo de música diferenciado, mas que ainda envolve os estudantes da instituição, sendo que alguns dos integrantes do Grupo de Metais são alunos do curso de Química. Para saber mais sobre a ação do Corpo de Bombeiros Mirins, acesse: www.cbvj.com.br/contato/mirim\r<br />\r<br />Toda a comunidade interna da instituição está convidada a participar do Almosom, seja como ouvinte ou mostrando seus talentos. Para se inscrever, basta procurar a professora Jaqueline (Artes) ou a CECOM.', 'Almosom', 0, 1),
(62, 'Comprar o notebook Acer VX5 vale a pena? Conheça a ficha técnica e preço', 'O Acer Aspire VX5 teve lançamento em abril de 2017 e chegou com uma opção do mercado de notebooks gamers de entrada. O aparelho segue a fórmula que combina processadores quad-core rápidos com placas de vídeo dedicadas mais básicas de AMD e Nvidia. Oferecido em duas versões no momento, o Aspire VX5 tem preços que giram dos R$ 5 mil aos R$ 6 mil na loja oficial da fabricante.\r<br />Nessa faixa de valor e ficha técnica, o notebook da Acer encontra uma competição pesada contra o Odyssey da Samsung e do Inspiron Gaming, da Dell. Se você está em dúvida se vale a pena comprar o Acer Aspire VX5, é interessante analisar suas especificações para conhecer bem pontos fortes e fracos do computador.\r<br />Dell ou Samsung: quem tem o melhor notebook gamer por até R$ 4 mil.\r<br />\r<br />A Acer apresenta duas versões do notebook no momento. Na mais simples, o computador vem com um processador de sétima geração Core i5 7300HQ de 2.5 a 3.5 GHz, 8 GB de RAM DDR4, disco rígido de 1 TB e placa gráfica Geforce GTX 1050 da Nvidia. Tudo isso com o preço de R$ 4.999 na loja oficial da marca. A versão superior tem processador quad-core no i7 7700HQ, 16 GB de RAM DDR4, disco de 1 TB e placa de vídeo superior com a GTX 1050 Ti. O valor também sobe para R$ 5.999.\r<br />Em termos gerais, o modelo mais caro se destaca porque tem processador e placa de vídeos superiores. O i7 é mais rápido (opera entre 2.8 e 3.8 GHz) e a 1050 Ti tem performance gamer melhor do que a 1050 convencional. Além disso, o dobro de memória RAM tende a deixar o PC mais esperto e tornar mais confortável o uso de vários aplicativos e jogos ao mesmo tempo.\r<br />\r<br />Mesmo a versão de entrada, por conta do processador i5 de quatro núcleos, o aparelho acaba fazendo bonito. Na comparação com notebooks comuns e ultrabooks, o processador tende a levar clara vantagem por conta de ser quad-core num panorama dominado por dual-cores. Outro ponto é a placa de vídeo: a GTX 1050, embora inferior à versão Ti do modelo mais poderoso, dá conta de jogos e tem fôlego suficiente para agradar usuários menos exigentes que pretendem jogar com o laptop.\r<br />\r<br />Ficha técnica do Acer Aspire VX5\r<br />Preços: R$ 4.999 e R$ 5.999\r<br />Tela: 15,6 polegadas, Full HD (1920 x 1080 pixels)\r<br />Processador: Core i5 7300HQ ou i7 7700HQ\r<br />Memória RAM: 8 ou 16 GB DDR4\r<br />Placa de vídeo: Geforce GTX 1050 ou GTX 1050 Ti\r<br />Armazenamento: disco rígido de 1 TB\r<br />Bateria: 2.800 mAh\r<br />Portas e interfaces: duas USB 3.0, uma USB 2.0, uma USB-C, Ethernet, HDMI, fone de ouvido/microfone, leitor de cartões, Bluetooth e Wi-Fi\r<br />Câmera: HDR de resolução 1280 x 720 pixels\r<br />Dimensões: 389 x 265.5 x 28.9 mm\r<br />Peso: 2,5 kg', '#informatica #dell #notebook', 2, 2),
(63, 'O que é a realidade mista do Windows 10', 'A realidade mista foi criada pela Microsoft com o objetivo de transformar o uso do Windows 10 em uma experiência em terceira dimensão — ou seja, 3D. Com os aparelhos necessários (computador, headset e sensor de movimento compatíveis), o usuário se transporta para um mundo virtual e pode utilizar o Windows em realidade virtual, sobrepondo mais de um programa simultaneamente.\r<br />\r<br />Realidade mista vs realidade virtual\r<br />\r<br />A realidade virtual é uma tecnologia que simula um ambiente virtual realista. Essa simulação funciona como uma forma de emulação que procura &quot;enganar os sentidos&quot; do usuário (visão e audição) através de equipamentos oculares e/ou auriculares (óculos e fones). Dessa forma, a pessoa se sente presenciando fisicamente o que está sendo transmitido apenas de forma virtual.\r<br />\r<br />A proposta da Microsoft é trazer a experiência da realidade virtual para seu sistema operacional de computadores. Isso significa que os programas e aplicativos do Windows serão vivenciados em um ambiente virtual que simula experiências físicas — arrastar com as mã. No entanto, a grande novidade da realidade mista é que será possível usar os softwares de forma simultânea.\r<br />\r<br />Além disso, a realidade mista também traz um novo elemento: sensores de movimento, que buscam reproduzir também o sentido de toque das mãos. Esses dispositivos são segurados pelo usuário e trabalham junto com o headset (fone e óculos) para aprimorar a sensação de imersão.\r<br />Ou seja, não há uma diferença concreta entre ambas as realidades e, sim, uma convergência. Na prática, a realidade mista usa o conceito e a tecnologia da realidade virtual, que simula um novo mundo construído digitalmente, junto a convergência de softwares e sensores de movimento.\r<br />\r<br />Como a realidade mista vai funcionar\r<br />\r<br />A realidade mista chegou com a atualização Windows 10 Fall Creators Update. Na nova versão, a Microsoft lançou, embutido, o aplicativo Mixed Reality Viewer (microsoft.com/mixed-reality-viewer), que simula uma casa onde as paredes e decoração são softwares. Segundo a fabricante, a tecnologia vai contar com uma grande variedade de experiências, com jogos imersivos, vídeos em 360 graus, filmes e séries, bate papos, aplicativos de criatividade, eventos ao vivo e mais.\r<br />\r<br />Com a convergência de funções simultâneas, será possível, por exemplo, conversar com um amigo por Skype e participar de uma partida de um jogo de realidade virtual, ao mesmo tempo. Os aplicativos trabalhados nessa tecnologia podem ser obtidos na própria loja de apps do Windows 10.\r<br />Por ora, a empresa anuncia os seguintes softwares:\r<br />Free The Night — experiência para admirar o céu estrelado como se não houvesse luz;\r<br />Sky World — um jogo de construir cidades para dragões;\r<br />Space Pirate Trainer — game de guerras no espaço.\r<br />Além disso, há a promessa de desenvolvimento de uma simulação de Minecraft, jogo que já faz sucesso entre crianças em versão 2D. É possível encontrar aplicativos com a tecnologia na loja oficial da Microsoft (microsoft.com/store/collections/MixedReality).\r<br />\r<br />Compatibilidade\r<br />Para usar a realidade mista, é necessário ter um computador com Windows 10 atualizado e compatível com a tecnologia, além de um headset preparado para mesma. A própria Microsoft apresenta uma lista de notebooks preparados para essa tecnologia, com especificações de hardware para o melhor desempenho.\r<br />\r<br />São eles:\r<br />- Acer Predator 17;\r<br />- Lenovo Yoga 720;\r<br />- Dell Alienware 13;\r<br />- Cyberpower Gamer Ultra Desktop;\r<br />- HP OMEN 17 e;\r<br />- Lenovo Legion Y720\r<br />\r<br />No entanto, a fabricante avisa que outros computadores também são compatíveis, existe uma lista de especificações necessárias, conforme a tabela abaixo.\r<br />\r<br />Outra saída para verificar a compatibilidade é executar o aplicativo Windows Mixed Reality PC Check (microsoft.com/store/p/windows-mixed-reality-pc-check), uma ajuda da própria Microsoft.', '#realidadevirtual #windows10 #microsoft', 2, 2),
(64, 'Telemóveis Android identificam localização do utilizador mesmo com funcionalidade desativada', 'Os smartphones não precisam de ter um cartão SIM inserido ou de instalar qualquer app para enviarem os dados de localização para a Google\r<br />\r<br />Uma investigação da Quartz concluiu que a Google está a recolher dados sobre a localização dos smartphones Android mesmo quando essa funcionalidade está desativada. Saliente-se que as informações são recolhidas mesmo que não tivessem sido instaladas apps e que não houvessem um cartão SIM no terminal, basta que ele esteja ligado à Internet.\r<br />\r<br />Um porta-voz da Google explicou que, desde o início deste ano, as moradas das torres de comunicações móveis passaram a estar incluídas na informação enviada para os serviços da empresa que enviam notificações e mensagens. A companhia salienta, porém, que os dados nunca foram armazenados.\r<br />\r<br />A Google adianta ainda que já está a tomar medidas para resolver esta problemática que levanta questões relevantes sobre a privacidade dos consumidores e que a informação passará a deixar de ser enviada no final deste mês.', '#android #localizador #google', 2, 2),
(65, 'App para ler a Bíblia usada para disseminar vírus em dispositivos Android', 'O malware parece ter sido desenvolvido pelo grupo Lazarus e, segundo a Palo Alto Networks, tinha como alvos os utilizadores coreanos de dispositivos Samsung.\r<br />\r<br />Investigadores da McAfee e da Palo Alto Networks descobriram um novo malware que se fazia passar por uma app legítima para ler a Bíblia em coreano, mas que acabava por transformar os smartphones Android numa botnet, graças à presença de uma backdoor. Ambas as empresas de segurança são da opinião que, tendo em conta as semelhanças no código, este vírus deve ter sido desenvolvido pelo grupo de cibercriminosos conhecido por Lazarus, revela o The Next Web.\r<br />\r<br />Contudo, este é um dos poucos pontos em que McAfee e Palo Alto Networks parecem estar de acordo, já que as análises individuais de cada empresa chegaram a algumas conclusões diferentes. Por exemplo, a McAfee refere que a app legítima foi descarregada mais de 1300 vezes, mas que a versão adulterada nunca chegou a estar disponível na Google Play, algo de que a Palo Alto discorda.\r<br />\r<br />A Palo Alto Networks defende ainda que o malware tinha como alvo principal utilizadores coreanos com dispositivos Samsung. Já a McAfee opta por uma perspetiva mais geral e alerta para o facto do grupo Lazarus ter agora começado a operar no mundo mobile.', '#virus #biblia #mcafee #app', 2, 2),
(66, 'Caçador de bugs recebe ameaças depois de detetar vulnerabilidade da DJI', 'A fabricante de drones iniciou em agosto um programa de caça de bugs, semelhante aos que Google, Microsoft e outros fazem. No entanto, um caçador revela ter recebido ameaças depois de detetar uma vulnerabilidade.\r<br />\r<br />A história é contada por Kevin Finisterrer e rapidamente chegou ao top das mais vistas do Hacker News. O caçador de bugs revela que descobriu que a DJI tinha publicado no GitHub, acidentalmente, um certificado SSL que dava acesso a informação sensível dos clientes alojada nos servidores da empresa. Numa primeira abordagem, a DJI confirmou que a falha entrava no âmbito da caça aos bugs e que daria direito a uma recompensa de 30 mil dólares.\r<br />\r<br />No entanto, algum tempo depois, Finisterre viu-se a braços com um imbróglio com a equipa legal da DJI, que lhe enviou cartas a solicitar que não revelasse publicamente a falha ou que já tinha colaborado com a DJI de todo. Num programa de caça de bugs, os especialistas procuram, não só a recompensa monetária, como o reconhecimento e o crédito pela descoberta. Com a carta onde se referia ao Computer Fraud and Abuse Act, a DJI parece estar a intimidar Finisterre que optou por declinar a recompensa monetária e publicar a sua experiência.\r<br />\r<br />«Recomendamos que a DJI corrija a falha o mais rapidamente possível e não tente qualquer ação legal. Com base na informação que temos hoje, parece estar a haver um mal entendido e não haver qualquer má intenção», disse Jonathan Cran, o vice-presidente da Bugcrowd, uma plataforma especializada em programas de caça a bugs.\r<br />\r<br />Há empresas que, antes de lançar este tipo de iniciativas, exigem que os investigadores não revelem publicamente as falhas, mas essa informação é passada antes do programa estrear, o que não parece ter sido o caso da DJI.', '#drones #bugs #ameaça', 2, 2),
(68, 'FÁBRICA DE SOFTWARE: UM ENSAIO PROFISSIONAL COM RESULTADOS REAIS', '&quot;Tenho mesmo que aprender estas coisas? Para que eu preciso disso? Como vou usar isso no meu trabalho? Estas são perguntas recorrentes dentro do universo dos estudantes de diversas áreas e níveis de ensino. Uma proposta, dentro do IFC Araquari, fez com que isso fosse respondido no dia a dia de estudantes dos cursos da área de informática.&quot;\r<br />\r<br />Desde o ano de 2013 foi aberto um novo caminho para a formação acadêmica e profissional dos estudantes dos cursos de informática, do Instituto Federal Catarinense Campus Araquari. Abriram-se portas para uma formação mais abrangente e completa, que compreende a prática profissional, tal e qual no mercado de trabalho, só que mediada pelo ambiente de ensino. Esta possibilidade foi propiciada com a criação da Fábrica de Software, um espaço de aprendizado que compreende rotinas diárias de um profissional ligado às Tecnologias de Informação (TI) e executa projetos conforme demandas reais de parceiros.\r<br />\r<br />É possível afirmar que a Fábrica de Software serve como um “ensaio profissional” para os discentes dos cursos Técnico em Informática e do Bacharelado em Sistemas de Informação (BSI). A proposta deste ambiente é servir como um agente catalisador de experiências nas áreas de ensino, pesquisa e extensão. Isso, através do acompanhamento do processo de formação do aluno e antecipando seu contato com as atividades profissionais.\r<br />\r<br />Ao fazer uso de conceitos teóricos, a Fábrica orienta os alunos na adoção de técnicas e ferramentas que permitem analisar, avaliar, projetar, desenvolver e implantar soluções de TI. Portanto, um dos grandes trunfos desta proposta é a certificação de que os conceitos apresentados em sala de aula estão sendo aplicados para o desenvolvimento de soluções. Ou seja, se o conhecimento passado em sala não está sendo aplicado é possível verificar e adequar, tanto as aulas, quanto a execução dos projetos pelos estudantes.\r<br />\r<br />A ATUAÇÃO PRÁTICA DA FÁBRICA DE SOFTWARE\r<br />\r<br />Aspectos como gerenciamento de equipes, capacitação e até mesmo ações de cunho social são incentivadas e promovidas dentro do escopo de atuação da Fábrica de Software. Além disso, atua no sentido de proporcionar condições para a efetivação de parcerias entre o IFC e outras instituições, sejam elas de ensino, governamentais ou empresariais.\r<br />\r<br />Já são 13 projetos concluídos dentro a Fábrica de Software em seus quatro anos de existência, sejam com demandas internas, provenientes de parcerias com a Prefeitura de Joinville ou da Reitoria do IFC, por exemplo. Esta proposta educacional alcança toda a região, como é possível visualizar com os atuais projetos em construção, para o Detrans (Joinville), ISSEM (Jaraguá do Sul), Sistema de gestão de projetos, Análise de salto vertical, Portal do IFC (Reitoria) e Irrigação automática.\r<br />\r<br />Hoje, são 18 estudantes do ensino superior e três do ensino médio atuando diretamente nos seis projetos em fase execução. Estes estudantes são coordenados por um “gerente”, que também é aluno dos cursos da instituição, e tem por função acompanhar o andamento de todas as propostas aprovadas para execução. Além disso, cada projeto possui um professor responsável pela orientação. Para o gerenciamento de toda a estrutura da Fábrica de Software, há um colegiado de professores responsáveis, composto pelos docentes do curso de Bacharelado em Sistemas de Informação.\r<br />\r<br />O ambiente de trabalho é constituído por 20 computadores, dois kits arduíno, uma estação meteorológica, uma impressora 3D e quatro tablets. Toda a estrutura deste ambiente de ensino gira em torno de força intelectual e de trabalho dos próprios integrantes da comunidade interna do IFC Araquari, utilizando exclusivamente tecnologia de softwares livres para o desenvolvimento de soluções.\r<br />\r<br />Prova da efetividade de ações como esta é uma taxa 40% maior de estudantes que, após passar pela Fábrica de Software, concluem o curso superior no IFC, comparado àqueles que não tiveram esta experiência. Propostas como esta mostram como os conhecimentos podem ser utilizados na prática, sem custos extras para formação profissional e gerando bons resultados para o ambiente estudantil e também para as empresas que recebem seus projetos em funcionamento.', '#info #software', 4, 2),
(69, 'Comunicado sobre fornecimento do café da manhã', 'Prezados estudantes, familiares/responsáveis e demais membros da comunidade escolar,\r<br />\r<br />Tendo em vista as discussões acerca do cumprimento e fiscalização da norma que trata do fornecimento de café da manhã somente para estudantes da Moradia Estudantil, também com base nos apontamentos trazidos por representantes de estudantes do Ensino Médio Técnico através do Grêmio Estudantil, além das manifestações dos estudantes, familiares e servidores durante reunião realizada em 30/10/2017, a Direção do campus Araquari vem informar que, após deliberação com a equipe, decidiu que:\r<br />\r<br />- Em 2017, após melhor organização da prestação de serviços no refeitório no período noturno, será possível manter a oferta do café da manhã para todos os estudantes dos Cursos Técnicos Integrados ao Ensino Médio que necessitarem desse serviço, no horário das 07h às 07h50, especificamente nos dias de semana letivos (não sendo servido em sábados letivos);\r<br />- Em 2018 o café da manhã será servido aos estudantes da Moradia Estudantil e será verificada a possibilidade de oferta para aqueles em situação de vulnerabilidade socioeconômica, segundo critérios a serem definidos pela Coordenação Geral de Assistência Estudantil/DDE/DG;\r<br />- Será mantida a fiscalização da norma que trata do jantar ser gratuito somente para estudantes da Moradia Estudantil;\r<br />- A instituição reforçará a busca por estratégias para acessar e utilizar os recursos do Programa Nacional de Alimentação Escolar – PNAE, em 2018.\r<br />\r<br />A Direção agradece pelas manifestações e se coloca, como sempre, aberta ao diálogo e a atender da melhor forma as necessidades e prioridades da comunidade.', '#querocafe #cafe #almoço', 4, 2),
(70, 'Professores Elegem Novo Coordenador do Curso Técnico em Agropecuária', 'O Colegiado do Curso Técnico em Agropecuária elegeu na última quinta-feira, dia 05 de outubro, o novo Coordenador do Curso para o biênio 2017/2019. A eleição contou com a participação de grande parte dos professores que representam o colegiado. Participaram como candidatos os professores Edvanderson Ramalho dos Santos e Fernanda Guimarães de Carvalho.\r<br />\r<br />Após o período de campanha e análise das propostas, o colegiado escolheu como Coordenador o Professor Edvanderson Ramalho dos Santos, com 65% dos votos válidos.\r<br />\r<br />Dentre as propostas do Professor Edvanderson para a sua gestão à frente da coordenação do curso estão, o fortalecimento do NDB e do colegiado como instâncias consultivas e deliberativas; a consolidação, em fórum participativo, das mudanças estruturais e pedagógicas do PPC do curso, visando à reformulação do Ensino Médio; manter a transparência em relação à agenda da coordenação, bem como o convite para que os docentes realizem a gestão compartilhada, além da proposta de encaminhamento em relação ao PPC do curso, de um projeto integrador que respeite às especificidades das disciplinas envolvidas em cada ano, prezando pela integração entre as disciplinas técnicas e as da área de formação geral.\r<br />\r<br />Desejamos ao Professor Edvanderson sucesso em mais este desafio.', '#sueli #coordenador #agro', 5, 2),
(71, 'Certificados da SEPE estão disponíveis para download', 'Após o fechamento da II Semana de Ensino, Pesquisa e Extensão do IFC Araquari, os participantes podem ter seu acesso individual a certificação de todas as atividades em que estiveram presentes.\r<br />\r<br />Os certificados estão disponíveis na página do participante, no endereço: even3.com.br/sepe2017 e podem ser baixados conforme as atividades realizadas em todos os dias do evento.\r<br />\r<br />Cada documento é individual e gerado através de um sistema totalmente automatizado, no qual os dados do participante são retirados diretamente de seu perfil online. Portanto, caso a certificação tenha algum equívoco no nome, o próprio participante pode alterar em seu perfil (clicando em seu nome, no canto direito superior, no site do evento).\r<br />\r<br />A certificação, nesta edição da SEPE, está diretamente atrelada à avaliação do evento. São algumas questões básicas, de múltipla escolha, para que os participantes deem sua opinião sobre pontos relacionados à organização do evento. Esta pesquisa tem como intuito verificar a aceitação do público para áreas específicas, gerando um feedback para que as próximas edições do evento possam ser aprimoradas.\r<br />\r<br />Quaisquer dúvidas sobre a certificação do evento podem ser enviadas diretamente para o e-mail raquel.rybandt@ifc.edu.br.', '#sepe #download', 4, 2),
(72, 'Atletas de Araquari tem um ótimo desempenho no JEASC', 'Na última semana, entre os dias 28 e 30 de setembro de 2017, o Instituto Federal Catarinense Campus Camboriú foi sede dos Jogos Abertos das Escolas Agrotécnicas de Santa Catarina – JEASC.  Em sua 11ª edição, o JEASC é uma iniciativa bienal que visa a integração das escolas agrotécnicas do estado, através da promoção do esporte como ideia central.\r<br />\r<br />O JEASC, ganha representatividade com a participação e envolvimento cada vez maior por parte de alunos e servidores que jogam suas respectivas instituições de ensino, sendo elas: CEDUP – Água Doce; CEDUP – Campoerê; CEDUP – Canoinhas; CEDUP – São José do Cerrito; CEDUP – São Miguel do Oeste; Colégio Agrícola La Salle – Xanxerê; IFC – Araquari; IFC – Camboriú; IFC – Concórdia; IFC – Rio do Sul; IFC – Santa Rosa do Sul; IFC – Videira.\r<br />\r<br />No ano de 2017 o IFC Araquari levou representantes para diversas modalidades, conquistando muitas premiações nas disputas em que esteve envolvido, foram elas:\r<br />1º lugar – Vôlei feminino\r<br />1º lugar – Canastra feminino\r<br />1º lugar – Dominó (bicampeão)\r<br />2º lugar – Truco\r<br />2º lugar – Tênis de mesa masculino\r<br />2º lugar – Xadrez\r<br />5º lugar – Basquete masculino\r<br />5º lugar – 800 metros rasos masculino\r<br />O desempenho dos atletas foi excelente, de acordo com os professores que acompanharam as equipes, o que representa o envolvimento e dedicação dos estudantes com os jogos. Eles treinam desde o início do ano e se comprometem a dar o seu melhor durante as competições.\r<br />\r<br />Após o encerramento dos jogos e premiação dos atletas, definiu-se também que a próxima instituição a sediar o JEASC será o IFC Araquari, no ano de 2019.', '#jeasc #ifcaraquari #premiação #atletas #esportes', 5, 2),
(73, '“O exemplo que a Sepe pode dar para as pessoas é de que a edação é o caminho para um Brasil melhor”', 'Jonas Cunha Espíndola, diretor do campus do Instituto Federal Catarinense (IFC) de Araquari, participou do Painel de Integração na tarde desta sexta-feira (15) e concedeu entrevista à equipe de reportagem do De Olho no Campus. Para o diretor, a Semana de Ensino, Pesquisa e Extensão (SEPE) é importante para a integração dos alunos dos ensinos médio e superior. A expectativa do diretor é que de quatro a cinco mil pessoas participem dos sete dias de evento.\r<br />\r<br />“O objetivo do evento é integralizar todas as ações de ensino, divulgar isso para a comunidade, mostrar o que nossos alunos estão fazendo, valorizar a pesquisa e a extensão desde o Ensino Médio até o Ensino Superior. Porque no Ensino Superior é quase que um obrigação ter pesquisa e extensão, mas no Ensino Médio não são todas as escolas que fazem. E nós fazemos aqui”, destacou Jonas.\r<br />\r<br />O diretor afirmou que este ano a direção decidiu dedicar um dia especial para as famílias, atendendo um pedido feito nas últimas edições do SEPE. Ele acredita que a SEPE é um evento que pode dar o exemplo para um país melhor.\r<br />\r<br />“O exemplo que a SEPE pode dar para as pessoas é que a educação é o caminho para um Brasil melhor. É através da educação que as pessoas vão conseguir fazer uma gestão mais honesta, vão conseguir fazer um Brasil melhor”, defendeu', '#sepe #jonas #nuncanemvi #brasilmelhor', 5, 2),
(74, 'Novos elementos da tabela periódica são aprovados', 'A tabela periódica reúne os elementos químicos classificados em função de sua composição e suas propriedades.\r<br />\r<br />Os nomes propostos em junho para quatro novos elementos da tabela periódica foram aprovados nesta quinta-feira, incluindo o Nihonium, que ocupa o lugar 113, e o Moscovium, cujo número atômico é 115. O instituto japonês de pesquisas Riken celebrou a aprovação do Nihonium, uma referência à palavra Nihon, que significa Japão e tem como símbolo Nh.\r<br />\r<br />A existência do Nihonium, primeiro elemento colocado em evidência na Ásia, havia sido demostrada em três oportunidades entre 2004 e 2012 por Kosuke Morita, professor da Universidade de Kyushu (sudoeste do Japão). Além do Nihonium e do Moscovium (Mc), uma referência a Moscou e cuja paternidade corresponde a pesquisadores russos e americanos, a União Internacional de Química Pura e aplicada (Iupac) e a União Internacional de Física Pura e Aplicada (Iupap) aprovaram a denominação de outros dois elementos.\r<br />\r<br />Os elementos são o Tennessine, em homenagem aos institutos de pesquisas do Tennessee, nos Estados Unidos, com número 117 na tabela e cujo símbolo é o Ts, e o Oganesson (Og, 118), em referência ao físico nuclear russo Yuri Oganesián. Foram descobertos por laboratórios da Rússia e Estados Unidos, segundo um comunicado da (Iupac).\r<br />\r<br />Novos elementos\r<br />\r<br />A tabela periódica dos elementos, também conhecida como tabela de Mendeleyev, em homenagem ao russo Dmitri Mendeleyev, que criou a primeira versão em 1869, reúne os elementos químicos classificados em função de sua composição e suas propriedades.\r<br />\r<br />Um elemento químico representa um conjunto de átomos que têm a mesma quantidade de prótons em seu núcleo. A tabela periódica ordena e agrupa os elementos conforme determinadas características, permitindo a cientistas prever inúmeras propriedades e reações.\r<br />\r<br />Os elementos descobertos são sintéticos e criados em aceleradores de partículas, que fazem átomos se chocarem e seus núcleos se fundirem. Os quatro são radioativos e permanecem estáveis apenas por frações de segundo antes de decair, liberando energia de seu núcleo e se transformando em outros elementos.', '#elementos', 3, 2),
(75, 'Cinco razões por que as máquinas moleculares ganharam o Nobel', 'Trio de cientistas ganhou o Nobel de Química nesta terça-feira pela criação das “menores máquinas do mundo”. Saiba por que são importantes\r<br />\r<br />Um trio de cientistas responsável pela criação de máquinas moleculares, as “menores máquinas do mundo”, ganhou o prêmio Nobel de Química nesta terça-feira. O francês Jean-Pierre Sauvage, o britânico J. Fraser Stoddart e o holandês Bernard Feringa desenvolveram motores, eixos, correntes, elevadores e até um “veículo” em escala molecular, que podem ter diversas aplicações futuras em nanotecnologia e biociência – essas máquinas poderiam, por exemplo, fazer parte da próxima geração de computadores ou levar medicamentos às células para combater o câncer.\r<br />\r<br />Os três pesquisadores, que vão dividir o prêmio de 8 milhões de coroas suecas (933.000 dólares ou mais de 3,5 milhões de reais) concedido pela Academia Real de Ciências da Suécia, contribuíram de maneiras diferentes para a criação das máquinas. Em 1983, Sauvage deu o primeiro passo nas pesquisas, ao conseguir transformar a ligação química entre duas moléculas em uma ligação “mecânica”. Esse substituição foi fundamental pois, para que uma máquina seja capaz de executar tarefas, suas diferentes partes precisam se mover em relação às outras – algo que as fortes ligações covalentes, que ocorrem naturalmente entre as moléculas, não são capazes de fazer.\r<br />\r<br />Essa foi a base sobre a qual Stoddart e Feringa trabalharam, criando as primeiras estruturas sintéticas capazes de transformar energia em diversos tipos de movimentos controláveis.\r<br />\r<br />Mas por que esses estudos são considerados um importante avanço científico? Veja abaixo cinco razões por que as “menores máquinas do mundo” podem revolucionar a ciência e medicina:\r<br />\r<br />1. Elas foram algumas das primeiras “máquinas inteligentes”\r<br />\r<br />As células dos seres vivos funcionam como motores, recebendo energia e executando funções como transmitir informações, regular temperatura, reparar danos ou fazer órgãos funcionarem. A ideia do trio de cientistas que ganhou o Nobel foi replicar esse trabalho em estruturas sintéticas – as máquinas criadas por eles conseguem, a partir da adição de energia, realizar tarefas, como movimentos controláveis. Assim, esses minúsculos dispositivos podem ser usados para a criação de materiais capazes de se “auto reparar” ou de executar funções sozinhos, por meio de estímulos externos.\r<br />\r<br />2. São 1.000 vezes menores que a espessura de um fio de cabelo\r<br />\r<br />As máquinas moleculares não podem ser vistas a olho nu. Minúsculos, esses motores, roldanas e correntes são um incrível avanço para a área da nanotecnologia, principalmente na área de biociência e informática. No futuro, invenções feitas a partir dessas máquinas poderão, por exemplo, levar medicamentos a pontos específicos do organismo.\r<br />\r<br />3. Funcionam como peças que poderão fazer parte de sofisticados nanorobôs\r<br />\r<br />“Em termos de desenvolvimento, o motor molecular está no mesmo estágio em que o motor elétrico estava em 1830, quando os cientistas exibiam diversas rodas e manivelas sem saber que elas levariam a trens elétricos, máquinas de lavar, ventiladores e processadores de alimentos. Máquinas moleculares serão provavelmente utilizadas no desenvolvimento de novos materiais, sensores e sistemas de armazenamento de energia”, afirma o comunicado da Academia Real de Ciências da Suécia.\r<br />\r<br />4. Seu impacto pode ser comparado à invenção do microchip\r<br />\r<br />Segundo comunicado da Academia Real de Ciências da Suécia, “os laureados deste ano miniaturizaram máquinas e conduziram a química a uma nova dimensão”. A criação de uma estrutura em dimensões reduzidas, o microchip, revolucionou a comunicação em todo o globo. Segundo o comitê do Nobel, as pequenas máquinas criadas por Sauvage, Stoddart e Feringa, apesar de ainda estarem em estágio inicial, poderão também promover imensas transformações na tecnologia.\r<br />\r<br />5. Elas poderão revolucionar a medicina e informática\r<br />\r<br />Em entrevista após o anúncio do prêmio, Feringa sugeriu ao comitê do Nobel que as máquinas moleculares podem ser usadas como nano robôs que entregarão medicamentos para células com câncer ou na criação de novos materiais inteligentes que consigam se adaptar ou mudar de acordo com o ambiente.', '#premio nobel', 3, 2),
(76, 'EUA proíbem sabonetes antibacterianos por riscos à saúde', 'Agência americana proibiu 19 ingredientes presentes em sabonetes antibacterianos. Empresas têm até 1 ano para tirar o produto do mercado\r<br />\r<br />A agência americana de medicamentos e alimentos (FDA, na sigla em inglês) proibiu a utilização de 19 ingredientes químicos nos sabonetes antibacterianos comercializados no país. De acordo com o órgão, essas substâncias – entre as quais estão o o triclosan, comum em sabonetes líquidos, e o triclocarban, presente na maior parte dos sabonetes em barra -, além de não terem se mostrado mais efetivas que os tradicionais, podem trazer danos para a saúde, como problemas ao sistema imunológico, propiciam a resistência bacteriana e estão associadas ao desenvolvimento de alguns tipos de câncer. \r<br />\r<br />“Os consumidores podem pensar que os sabonetes antibacterianos são mais efetivos para evitar os germes, mas não existe evidência científica de que sejam melhores que água e sabão comum. Alguns dados, ainda por cima, sugerem que os ingredientes antibacterianos podem fazer mais mal do que bem em longo prazo.”, disse Janet Woodcok, diretora da divisão de drogas da FDA.\r<br />\r<br />Segundo Theresa Michele, que trabalha na área de cuidados pessoais da FDA, 40% dos produtos não médicos que estão no mercado contêm ao menos um dos ingredientes agora proibidos. Os fabricantes têm um ano para cumprir as novas regras e, inclusive, alguns já começaram a retirar de seus produtos os ingredientes em questão. A proibição não inclui gel antisséptico, lenço umedecido nem produtos antibacterianos utilizados em hospitais e centros de saúde.\r<br />\r<br />Apesar da proibição, a FDA ressalta a importância de lavar as mãos com sabonete para prevenir doenças. “Lavar as mãos com água e sabão é um dos passos mais importantes que um consumidor pode tomar para evitar doenças e prevenir a transmissão de germes a outras pessoas”, indicou o comunicado da agência.\r<br />\r<br />Leia também:\r<br />EUA aprovam novo teste caseiro de fertilidade masculina\r<br />Gordura trans será banida dos EUA em três anos\r<br />\r<br />Em 2013 a agência havia pedido que os fabricantes enviassem evidências comprovando a segurança e a efetividade desses ingredientes. Entretanto, a FDA disse que o material recebido não foi satisfatório em relação a 19 componentes presentes nesses produtos. Outros três compostos – cloreto de benzalcônio, cloreto de benzetônio e cloroxilenol – ainda estão sob análise da agência e por enquanto não foram  proibidos.\r<br />\r<br />O Instituto Americano de Limpeza (ACI, na sigla em inglês), que representa os fabricantes, afirma que estes sabonetes são seguros. “Os sabonetes antibacterianos são chave para a saúde devido à importância das mãos limpas na prevenção de infecções. Lavar as mãos com sabonetes antissépticos pode ajudar a reduzir o risco de infecção, mais que a lavagem com água e sabonete não antibacteriano.”, disse o ACI em um comunicado.\r<br />\r<br />Por outro lado, o Grupo de Trabalho Ambiental, aplaudiu o anúncio da proibição. “Esta decisão da FDA é uma importante vitória para a saúde humana e o meio ambiente”, disse Ken Cook, cofundador e presidente deste grupo, que defendia há uma década esta proibição.\r<br />\r<br />Brasil\r<br />\r<br />Em 2012 a Proteste avaliou a eficácia de 12 sabonetes para acabar com quatro tipos de bactérias: Escherichia coli presente no intestino grosso e nas fezes humanas, Pseudomonas aeruginosa, causadora da infecção hospitalar, Serratia marcescens, que ataca o sistema urinário e respiratório e a Staphylococcus aureus que causa infecções na pele e até pneumonia.\r<br />\r<br />Oito sabonetes eliminaram a bactéria Escherichia coli, presente no intestino grosso e nas fezes humanas. Dos cinco sabonetes que anunciavam proteger a pele contra o S. aures, somente o Dettol em barra confirmou sua ação. Protex e Lifebuoy, nas versões sólidas, garantem eliminar a S. marcescens, mas só o Lifebuoy conseguiu. Embora não indiquem ação bactericida específica em seus rótulos, Granado Antisséptico, Ypê e Racco e Protex demonstraram ação antibacteriana. Por sua vez, o Protex, que afirma eliminar 99,9% das bactérias presentes na pele, não cumpriu o prometido.\r<br />\r<br />Na época, a  Proteste pediu à Anvisa a padronização dos testes de eficácia bactericida realizados pelos fabricantes.\r<br />\r<br />Anvisa', '#anvisa', 3, 2),
(77, 'Falta de chuva provoca redução de 10% no leite e 20% no trigo', 'Estimativa da Epagri/Cepa calcula que tempo seco levou a redução de 23% nas áreas plantadas do cereal em Santa Catarina\r<br />\r<br />A falta de chuva já traz prejuízo no campo com perdas de 10% no leite e previsão de queda de 20% na produção de trigo em Santa Catarina. A estimativa faz parte de levantamento preliminar do Centro de Socioeconomia e Planejamento Agrícola da Epagri (Epagri/Cepa) e da Secretaria Estadual da Agricultura.\r<br />\r<br />- No primeiro semestre, a produção de leite aumentou 8% mas, em julho, houve uma queda de 10% em relação ao volume previsto, em virtude da estiagem - afirma o secretário-adjunto de Agricultura do Estado, Airton Spies.\r<br />\r<br />\r<br />Em relação ao plantio de trigo, os dados do Cepa/ Epagri já indicam uma queda de 23% na área plantada, que sofreu retração de 69 mil hectares para 53 mil hectares. De acordo com o engenheiro Agrônomo João Rogério Alves, essa redução se deve ao baixo preço na época do plantio, aliado à estiagem. Com isso, os dados de junho já previam uma queda de 24% na produção, que foi de 229 mil toneladas no ano passado, baixando para 175 mil toneladas. Com a seca de julho, a queda de produção pode ser ainda maior.\r<br />\r<br />O gerente comercial da Cooperativa Agroindustrial Alfa, que abrange mais de 80 municípios do Oeste e Planalto Norte, Lourenço Lovatel, estima que a redução na área plantada pode chegar a 30%, com quebra de mais 20% por falta de umidade. O presidente da Companhia Integrada para o Desenvolvimento Agrícola de Santa Catarina (Cidasc), Enori Barbieri, prevê que a quebra será ainda maior.\r<br />\r<br />– Muitos produtores da região de Campos Novos desistiram de plantar, porque o solo está seco e isso deve aumentar a redução de área plantada para próximo de 30%. Isso somado à falta de umidade para o desenvolvimento das lavouras pode levar a uma redução de até 50% na produção – avalia.\r<br />\r<br />O produtor Gilcemir Piaia, de Chapecó, é um dos que já estima as perdas em 30%. Ele plantou 400 hectares. Em 100 hectares, a produção já está praticamente perdida, pois as sementes não germinaram. No restante, a produtividade estimada, será de 30 sacas por hectare, contra 55 do ano passado.\r<br />\r<br />– O trigo já foi plantado há 40 dias, deveria estar com o dobro de tamanho, em novembro vamos saber o tamanho do prejuízo – afirmou o produtor.\r<br />\r<br />Aumento na importação do cereal\r<br />\r<br />Outro produtor de Chapeco, André Foleto, também reclamou que germinou somente entre 60 a 70% das sementes. Ele plantou 90 hectares e já prevê uma quebra de 30 a 40%. Com a previsão de quebra o preço da saca, que estava em R$ 33 na época do plantio, já chegou a R$ 38.\r<br />\r<br />– Há uma previsão de quebra em toda a região Sul e com isso o Brasil terá que importar mais trigo, o que vai impactar nos preços para o consumidor – avalia o gerente comercial da Cooperalfa, Lourenço Lovatel. O Brasil consome 11,5 milhões de toneladas e produz somente metade do que consome, em anos normais. Agora, deve importar do exterior 2/3 do consumo.\r<br />\r<br />No leite não deve haver impacto direto no preço pois agora é o período de maior produção. Além disso a pastagem tem recuperação rápida. Mas outras culturas também estão sendo afetadas, segundo o secretário adjunto Airton Spies.\r<br />\r<br />– Há perdas também na bovinocultura de corte, em virtude das pastagens, no tabaco e na cebola, pois é época de transplantar as mudas, então vai haver atrasos, e também na banana – destaca Spies.\r<br />\r<br />Mesmo assim, o secretário-adjunto afirma que o impacto é bem menor do que se a falta de chuva ocorresse no período de verão.', '#pecuaria', 1, 2);
INSERT INTO `noticias` (`id_noticia`, `titulo_noticia`, `texto_noticia`, `tags_referencia`, `cod_categoria`, `id_usuario`) VALUES
(78, 'Federação da Agricultura e Pecuária promove programa de assistência técnica', 'Atenta às dificuldades e às potencialidades de Santa Catarina no mercado de bovinos, a Federação da Agricultura e Pecuária de Santa Catarina (Faesc) desenvolveu o programa Assistência Técnica e Gerencial (ATeG) da Pecuária de Corte. Feita em parceria com o Sebrae-SC e com o Serviço Nacional de Aprendizagem Rural (Senar-SC), a iniciativa dá apoio gratuito a produtores rurais, de pequenos a médios. Com cinco meses de operação, atende 600 pecuaristas em 22 municípios, principalmente na região serrana e no Oeste, que dividem a importância na pecuária de corte no Estado. \r<br />\r<br />Na avaliação do coordenador do programa, o vice-presidente de finanças da Faesc, Antonio Marcos Pagani de Souza, a alta procura revela a demanda reprimida por assistência no segmento.\r<br />\r<br />– Nosso objetivo é proporcionar maior renda ao produtor rural, desenvolvendo o potencial que temos neste setor – diz.\r<br />\r<br />Pecuarista há 55 anos, Antônio Santa Catarina entrou para a ATeG. É a primeira vez que busca esse tipo de assistência. \r<br />\r<br />– Quando comecei, não tinha nem veterinário, então sempre fiz tudo sozinho. Acho que isso é uma coisa boa, que pode ajudar. Quero inclusive fazer uma contabilidade para calcular depreciação da propriedade e coisas assim – diz o produtor rural.\r<br />\r<br />A própria Faesc contratou 24 técnicos, entre engenheiros agrônomos e médicos veterinários, que fazem visitas às propriedades. Toda a cadeia produtiva é assistida: genética, manejo adequado, melhoria da alimentação e das instalações. A meta é que em dois anos sejam inseminadas 50 mil matrizes (animais que servem à reprodução).\r<br />\r<br />O superintendente do Senar-SC, Gilmar Antônio Zanluchi, reforça que a ATeG não só prepara os produtores rurais para as atividades pecuárias, mas melhora a visão empresarial. Conforme a supervisora estadual do programa, Paula Nunes, todos os dados gerenciais coletados são lançados em um software, utilizado nacionalmente, que abriga informações completas das propriedades.\r<br />\r<br />– Com essas informações é possível fazer comparações e tomar decisões mais assertivas. A partir disso, os empresários rurais terão modelos para melhorar a sua rentabilidade – acredita.\r<br />\r<br />Para ter acesso aos serviços oferecidos pelo ATeG, o produtor deve procurar o sindicato rural de sua cidade.', '#pecuaria', 1, 2),
(79, 'Valor Bruto da Produção agropecuária de SC cresce 16,2% em 2016', 'Enquanto outros setores da economia sofrem com a crise, a agropecuária catarinense obteve um desempenho surpreendente em 2016. Neste ano, o Valor Bruto de Produção (VBP) é estimado em R$28,8 bilhões, crescimento nominal de 16,2% em relação a 2015, e de 3,5% ao descontar a inflação do período. Em 2015, os preços ao produtor haviam aumentado 2,8% ante 2014, com uma inflação de quase 7%. Os dados foram divulgados nesta terça-feira, em coletiva de imprensa na sede da Epagri, na Capital.\r<br />\r<br />O cenário nacional, contudo, é bem diferente. O  VBP brasileiro deve alcançar R$ 519,3 bilhões, queda de 2,5% no paralelo com 2015, segundo estimativa do Ministério da Agricultura, Pecuária e Abastecimento (Mapa). \r<br />\r<br />O bom resultado em SC se deve ao aumento de preços em 12 dos 20 produtos mais importantes da agropecuária no Estado. Enquanto a quantidade produzida cresceu 1,60%, os preços subiram 14,40%.\r<br />\r<br />E o que provocou a queda nacional foi justamente um dos motivadores do bom desempenho catarinense. As secas, especialmente no Centro-Oeste e no Nordeste, tiveram efeitos nefastos para a safra de grãos. Com a oferta reduzida, os preços pagos aos produtores catarinenses aumentaram. \r<br />\r<br />Além disso, a forte desvalorização cambial, especialmente no primeiro semestre, favoreceu as exportações, que também cresceram por aqui. O Estado detinha 33,7% das exportações nacionais de produtos animais no ano passado (sendo mais de 23% de carne de frango) e outros 17,3% dos de origem vegetal.\r<br />\r<br />A pecuária respondeu por cerca de 60% do valor de produção catarinense neste ano, e o maior destaque foi o leite, com aumento de 40,8% no VBP, graças à queda na oferta brasileira observada desde 2015. O frango também teve um incremento importante, de 19%. \r<br />\r<br />Já a carne suína registrou queda de 1,8% por conta do menor valor dos animais abatidos. Os suinocultores também sofreram ao longo do ano com o aumento nos custos de produção, especialmente pelo encarecimento do milho. Apesar disso, as exportações de janeiro a novembro alcançaram U$S 481 milhões, enquanto para o mesmo período de 2015 somaram de U$S 389 milhões. \r<br />\r<br />Na agricultura, os altos preços compensaram a redução de volumes produzidos, tanto para grãos quanto para lavouras temporárias. Geada e excesso de chuvas prejudicaram diversas culturas. O milho e a mandioca, entretanto, tiveram menor produção por diminuição na área plantada. \r<br />\r<br />Para o Secretário de Estado da Agricultura e da Pesca, Moacir Sopelsa, o bom desempenho do segmento se deve, em parte, às exportações, impulsionadas pela abertura de novos mercados (como Japão e China). Só para a China, as vendas de carne suína cresceram 3000%, de acordo com dados da Federação das Indústrias de Santa Catarina (Fiesc).\r<br />\r<br />Projeção para 2017\r<br />\r<br />Para Tabajara Marcondes, da Epagri, dificilmente o bom resultado da agropecuária catarinense se repetirá, neste mesmo nível, em 2017, já que os preços estão esticados ao limite e os demais indicadores econômicos não são favoráveis. \r<br />\r<br />Em nível nacional, de acordo com o Mapa, é previsto crescimento real do VBP de 8,3% para o ano que vem. A projeção é atribuída, principalmente, às perspectivas favoráveis de algodão, feijão, milho, soja e arroz.', '#pib', 1, 2),
(80, 'RenovaBio será avaliado em regime de urgência na Câmara', 'Câmara dos Deputados aprovou, nesta quarta-feira (22/11) a urgência para avaliação da Política Nacional de Biocombustíveis (RenovaBio). O pedido recebeu 299 votos favoráveis e 9 contrários. A partir disso, a projeto de lei apresentado pelo deputado Evandro Gussi (PV-SP) deve ser apreciado mais rapidamente pelos deputados.\r<br />\r<br />Protocolado no último dia 14, o projeto define as normas de incentivo à produção de combustíveis renováveis no Brasil. Entre os objetivos, estão o cumprimento das metas do Acordo do Clima de Paris e o aumento da participação desse tipo de combustível na matriz energética nacional.\r<br />\r<br />Na justificativa para o projeto, o parlamentar ressaltou a relevante participação brasileira no mercado global de biocombustíveis, sendo o segundo maior produto. No entanto, avaliou que faltam bases para incentivar seu uso no país.', '#diesel', 1, 2),
(81, 'Brasil aumentou importação de cebola em outubro', 'Brasil importou 1,74 mil toneladas de cebola no mês de outubro, superando em 39,75% o volume trazido em setembro, que foi de 1,245 mil toneladas. Em relação a outubro do ano passado, a quantidade é 81,81% maior. Um ano atrás, os importadores compraram 957 toneladas do produto.\r<br />\r<br />Os números são do Ministério da Agricultura (Mapa) e estão no mais recente Boletim Prohort, sobre o mercado de frutas e hortaliças, divulgado pela Companhia Nacional de Abastecimento (Conab). Apesar do resultado, na avaliação da Conab, a cebola de fora está com pouco espaço no mercado nacional, que tem privilegiado a oferta interna.\r<br />\r<br />“As importações continuam sem espaço no mercado, muito em função da oferta suficiente e da qualidade da cebola nacional e, consequentemente, dos níveis de preço deste ano”, diz a Conab, no relatório. (veja gráfico abaixo)\r<br />\r<br />Foi o quinto mês seguido de redução nas importações mensais de cebola em 2017. Em maio deste ano, as compras do produto de outros países tinham totalizado 26,831 mil toneladas. No entanto, também são cinco meses consecutivos em que o volume supera o registrado em um mês supera o do mesmo mês do ano anterior.\r<br />         A importação de cebolas tem causado preocupação nos produtores nacionais, de acordo com entidades representativas do setor. O assunto foi discutido em reunião da Frente Parlamentar Agropecuária (FPA) com o ministro da Agricultura, Blairo Maggi, em Brasília. Segundo a FPA, produtores brasileiros estão preocupados, especialmente, com a cebola holandesa, cujo preço estaria “muito mais baixo”.\r<br />\r<br />“A Holanda, responsável por 15% do mercado mundial em produção de cebola, é um país que não paga imposto e que o governo subsidia a agricultura. Não tem como competir, porque 85% da produção da cebola no Brasil vem de pequenos produtores das regiões Sul e Nordeste”, destaca o presidente da Associação Nacional dos Produtores de Cebola (Anace), Rafael Jorge Corsino, na nota da Frente Parlamentar.\r<br />\r<br />Diante da situação, a bancada ruralista informou ter pedido a inclusão da cebola na Lista de Exceções da Tarifa Externa Comum (Letec). Com isso, seria possível impor durante tempo determinado, tarifa sobre a importação, o que, na visão dos parlamentares, faria o custo de produção ficar igual ao da cebola brasileira.\r<br />\r<br />“É uma medida absolutamente correta que respalda o produtor rural brasileiro e garante a sustentabilidade do setor. É a valorização do nosso produto”, diz o deputado Alceu Moreira (PMDB-RS), conforme divulgado pela FPA.\r<br />\r<br />Ainda de acordo com a entidade, será realizado em parceria com a Empresa Brasileira de Pesquisa Agropecuária (Embrapa) um processo de industrialização para agregar valor à cebola nacional.\r<br />\r<br />Gosta das matérias da Globo Rural? Então baixe agora nosso aplicativo e receba alertas de notícias no seu celular, além de acessar todo o conteúdo do site e da revista. E se você quiser ler todas as publicações da Globo,', '#cebola', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `desc_tipo_usuario` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo_usuario`, `desc_tipo_usuario`) VALUES
(1, 'admin'),
(2, 'user_comum');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `senha` varchar(32) DEFAULT NULL,
  `nome` varchar(80) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `dt_nasc` date DEFAULT NULL,
  `id_tipo_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `email`, `senha`, `nome`, `cpf`, `dt_nasc`, `id_tipo_usuario`) VALUES
(1, 'lucasmartendal777@gmail.com', 'edc7a1e79a9b85e63c2ed5006339af89', 'Lucas Martendal', '111.111.111-11', '2000-08-08', 1),
(2, 'nicolasdsrosa@gmail.com', 'dc41f3159e3816b1534af636931f50bd', 'Nicolas dos Santos Rosa', '222.222.222-22', '1999-02-24', 1),
(3, 'bil.tambosi@gmail.com', '7aee8b2e09b83a2c403d2b3c13abf084', 'Gabriel Laurindo Tambosi', '333.333.333-33', '2000-05-28', 1),
(4, 'joao.francisco@gmail.com', '1a3a13d15f43486905394367e46e0fa7', 'João Francisco', '444.444.444-44', '1993-04-25', 2),
(5, 'mari@gmail.com', '7ebab61bf9bc2e4cb053cfa72e9d1a2e', 'Maria Tereza Fonseca', '555.555.555-55', '1990-11-30', 2),
(6, 'ze@gmail.com', 'fd6a7b4da9eda9ebc857dd599ec2b7ce', 'José da Silva', '777.777.777-77', '1996-10-17', 1),
(7, 'chico@gmail.com', '8a7c668215609ff03d8d9d1a302361fc', 'Chico Pereira', '888.888.888-88', '2002-01-16', 2),
(8, 'comum@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'gabriel usuario comum', '11005345988', '2000-05-28', 2),
(9, 'tambosi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'tambosi', '84566337987', '2000-05-28', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id_contato`);

--
-- Indexes for table `midia`
--
ALTER TABLE `midia`
  ADD PRIMARY KEY (`cod_midia`),
  ADD KEY `id_noticia` (`id_noticia`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`),
  ADD KEY `cod_categoria` (`cod_categoria`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_tipo_usuario` (`id_tipo_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id_contato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `midia`
--
ALTER TABLE `midia`
  MODIFY `cod_midia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `midia`
--
ALTER TABLE `midia`
  ADD CONSTRAINT `midia_ibfk_1` FOREIGN KEY (`id_noticia`) REFERENCES `noticias` (`id_noticia`);

--
-- Limitadores para a tabela `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticias_ibfk_1` FOREIGN KEY (`cod_categoria`) REFERENCES `categoria` (`cod_categoria`),
  ADD CONSTRAINT `noticias_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
