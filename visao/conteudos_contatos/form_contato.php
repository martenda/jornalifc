<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hora exata</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style type="text/css">

        .wrapper{
            //padding-top: 20px;
            padding-top: 50px;
        }

        input.parsley-error,
        select.parsley-error,
        textarea.parsley-error {
            border-color:#843534;
            box-shadow: none;
        }


        input.parsley-error:focus,
        select.parsley-error:focus,
        textarea.parsley-error:focus {
            border-color:#843534;
            box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
        }


        .parsley-errors-list {
            list-style-type: none;
            opacity: 0;
            transition: all .3s ease-in;

            color: #d16e6c;
            margin-top: 5px;
            margin-bottom: 0;
            padding-left: 0;
        }

        .parsley-errors-list.filled {
            opacity: 1;
            color: #a94442;
        }

    </style>

    </head>

    <body>



        <!-- Page Content -->
        <div class="container">

            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Contato e envio de mensagens</h1>
                </div>
            </div>
            <!-- /.row -->

            <!-- Content Row -->
            <div class="row">
                <!-- Map Column -->

                <!-- Contact Details Column -->
                <div class="col-md-8">
                    <h3>Contato</h3>


                    <label title="Email">E-mail's:</label> lucasmartendal777@gmail.com; nicolasdsrosa@gmail.com; bil.tambosi@gmail.com
                </p>
                <p><i class="fa fa-clock-o"></i>
                    <label title="Hours">Horário de atendimento:</label> Segunda à Sexta, das 8:00h às 12h e das 14h às 18h</p>

                </div>
            </div>
            <!-- /.row -->

            <!-- Contact Form -->
            <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
            <div class="row">
                <div class="col-md-8">
                    <h3>Envie sua mensagem</h3>


                    <form name="sentMessage" id="contactForm" novalidate action="../controlador/formularios/processa_contato.php" method="post">
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Motivo do contato:</label>
                                <input type="text" name="motivo_contato" class="form-control" id="motivo_contato" required data-validation-required-message="Please enter your name."
                                placeholder="Dúvida, sugestão, reclamação..." minlength="6">
                                <p class="help-block"></p>
                            </div>
                        </div>

                    <div class="form-group">
                                <label class="col-sm-4 control-label">Mensagem:</label>
                                 <div >
                                <textarea name="mensagem" rows="10" cols="100" class="form-control" id="mensagem" required data-validation-required-message="Please enter your message" minlength="5" maxlength="1000" style="resize:none"></textarea>
                            </div>
                        </div>

<br>
                        <div id="success"></div>
                        <!-- For success/fail messages -->
                        <button type="submit" class="btn btn-success">Enviar mensagem</button>
                    </form>
                </div>

            </div>
            <!-- /.row -->


        </div>
        <!-- /.container -->
<script type="text/javascript">
  $('#contactForm').parsley();
</script>


    </body>

    </html>
