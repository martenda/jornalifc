<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoUsuario();

  require_once './../controlador/controlador_mensagens.php';

  $id_contato = filter_input(INPUT_GET, 'contato');
  $detalhes = obterDetalhesContato($id_contato);

  if ($detalhes['id_resposta'] == 0) {
      $h1 = "Mensagem";
      $tipo_ctt = "mensagem";
      $resposta = obterResposta($detalhes['id_contato']);
  }else {
      $h1 = "Resposta";
      $tipo_ctt = "resposta";
  }

?>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ler <?= $h1 ?></h1>
        </div>
    </div>
    <!-- /.row -->

    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">

        <div class="col-md-8">

            <h3>Detalhes da <?= $tipo_ctt ?>:</h3>

            <p><i class="fa fa-phone"></i>
                <label title="Phone">#</label><?= $detalhes['id_contato'] ?>
            </p>
            <p><i class="fa fa-envelope-o"></i>
                <label title="Email">Nome</label>: <?= obterNomeContato($detalhes['id_usuario']); ?>
            </p>
            <p><i class="fa fa-clock-o"></i>
                <label title="Hours">E-mail</label>: <?= obterEmailContato($detalhes['id_usuario']); ?>
            </p>
            <p><i class="fa fa-phone"></i>
                <label title="Phone">Tipo</label>: <?= $detalhes['tipo_contato'] ?>
            </p>
            <p><i class="fa fa-clock-o"></i>
              <label title="Hours">Data</label>: <?= $detalhes['data_contato'] ?>
            </p>
            <p><i class="fa fa-envelope-o"></i>
                <label title="Email">Texto</label>:
                <p><?= $detalhes['texto_contato'] ?></p>
            </p>
            <?php
            if ($tipo_ctt == "mensagem") {

              if ($resposta == false) {
                  echo "
                  <br>
                  <br>
                  <p><i class='fa fa-clock-o'></i>
                  <label title='Hours'>Essa mensagem ainda não foi respondida...</label>
                  </p>
                  ";
              }else {
                  echo "
                  <br>
                  <br>
                  <p><i class='fa fa-clock-o'></i>
                  <label title='Hours'>Essa mensagem já possui uma resposta. Leia ela aqui:</label>
                  <a href="."home.php?navegacao=ler_mensagem&contato={$resposta[0]['id_contato']}".">
                  Resposta #{$resposta[0]['id_contato']}</a>.
                  </p>
                  ";
              }

            }else {
              echo "
              <br>
              <br>
              <p><i class='fa fa-clock-o'></i>
              <label title='Hours'>Essa é a resposta escrita para a</label>
              <a href="."home.php?navegacao=ler_mensagem&contato={$detalhes['id_resposta']}".">
              Mensagem #{$detalhes['id_resposta']}</a>.
              </p>
              ";
            }
            ?>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
