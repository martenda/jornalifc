<?php
  include_once '../controlador/controlador_noticias.php';
  require_once '../modelo/login.class.php';

  $titulo_noticia = filter_input(INPUT_GET, 'noticia');
  $noticia = obterNoticia($titulo_noticia);

?>

<!-- Page Content -->
<section class="container">

    <!-- Page Heading/Breadcrumbs -->
    <section class="row">
        <section class="col-lg-12">
            <h1 class="page-header"><?= $titulo_noticia ?></h1>
        </section>
    </section>
    <!-- /.row -->

    <!-- Project One -->

    <section class="row">

        <?php foreach (obterImagensNoticia($noticia['id_noticia']) as $imagens) : ?>

        <section class="col-md-4">
          <img class="img-responsive img-hover" src="./../<?= $imagens["caminho"]; ?>" alt="Imagem da Notícia" >
          <br>
        </section>

        <?php endforeach; ?>

        <section class="col-md-12">

            <br>

            <p> <?= $noticia['texto_noticia']; ?> </p>

            <br>

            <p>Autor: <?= obterAutorNoticia($noticia['id_usuario']); ?></p>

            <br>

            <p>Tag de referência: <a href="../controlador/formularios/processa_pesquisar_noticia.php?pesquisa=<?= $noticia['tags_referencia']; ?>"><?= $noticia['tags_referencia']; ?></p>


              <?php
              if ( isset($_SESSION['usuario']) && $_SESSION['usuario']['admin'] == true ) {
                  echo "<br>";

                  if ($noticia['cod_categoria'] != 5) {
                      echo "
                      <a class='btn btn-default' href="."./home.php?navegacao=atualizar_noticia&noticia=".$noticia['id_noticia'].">Atualizar</a>
                      ";
                      echo "
                      <a class='btn btn-default' href="."./../controlador/desativar_noticia.php?id_noticia=".$noticia['id_noticia'].">Desativar</a>
                      ";
                  }else{
                      echo "
                      <a class='btn btn-default' href="."./../controlador/reativar_noticia.php?id_noticia=".$noticia['id_noticia'].">Reativar</a>
                      ";
                  }
              }
              ?>


        </section>

    </section>
    <!-- /.row -->


    <!-- Pagination -->
    <section class="row text-center">
        <section class="col-lg-12">

        </section>
    </section>
