<?php
  require_once '../controlador/controlador_noticias.php';
  require_once '../controlador/controlador_categorias.php';
  require_once '../modelo/login.class.php';

  $id_noticia = obterDescCategoria( filter_input(INPUT_GET, 'noticias') );
  $noticia = obterNoticiaPesquisada($id_noticia);
//echo "<br>";var_dump($noticia);
?>

    <!-- Page Content -->
    <section class="container">

        <!-- Page Heading/Breadcrumbs -->
        <section class="row">
            <section class="col-lg-12">
                <h2 class="page-header">Encontramos estes<?//= $n_pesquisas ?> resultados:</h2>
            </section>
        </section>
        <!-- /.row -->

        <?php

        ?>
        <!-- Project One -->

        <section class="row">

            <section class="col-md-7">
              <img class="img-responsive img-hover" src="./../<?= obterImagensNoticia($noticia['id_noticia'])[0]["caminho"]; ?>"
                onError="this.onerror=null;this.src='./imgs/img_padrao_noticia.png';" alt="Imagem da Notícia">
            </section>


            <section class="col-md-5">
                <h3> <?= $noticia['titulo_noticia']; ?> </h3>

                <p> <?= substr($noticia['texto_noticia'], 0, 500); ?> [...] </p>

                <a class="btn btn-primary" href="home.php?navegacao=ler_noticia&noticia=<?= $noticia['titulo_noticia']; ?>">Ler mais</i></a>

                <?php
                if ( isset($_SESSION['usuario']) && $_SESSION['usuario']['admin'] == true ) {
                    if ($noticia['cod_categoria'] != 5) {
                      echo "
                      <a class="."btn btn-warning"." href="."./home.php?navegacao=atualizar_noticia&noticia=".$noticia['id_noticia'].">Atualizar</a>
                      <a class="."btn btn-danger"." href="."./../controlador/desativar_noticia.php?id_noticia=".$noticia['id_noticia'].">Desativar</a>
                      ";
                    }else{
                      echo "
                      <a class="."btn btn-warning"." href="."./home.php?navegacao=atualizar_noticia&noticia=".$noticia['id_noticia'].">Reativar</a>
                      ";
                    }
                }
                ?>

            </section>

        </section>
        <!-- /.row -->
        <hr>
        <?php //endforeach; ?>

        <p style="text-align: center;">Jesus te ama!</p>

        <!-- /.row -->
