<?php
  require_once '../controlador/controlador_noticias.php';
  require_once '../controlador/controlador_categorias.php';
  require_once '../modelo/login.class.php';

  $desc_cat = obterDescCategoria( filter_input(INPUT_GET, 'navegacao') );
  $cod_cat = obterCodCategoria($desc_cat);
?>

    <!-- Page Content -->
    <section class="container">

        <!-- Page Heading/Breadcrumbs -->
        <section class="row">
            <section class="col-lg-12">
                <h1 class="page-header"><?= $desc_cat ?></h1>
            </section>
        </section>
        <!-- /.row -->

        <?php foreach (listarNoticias($cod_cat) as $noticias) : ?>
        <!-- Project One -->

        <section class="row">

            <section class="col-md-7">
              <img class="img-responsive img-hover" src="./../<?= obterImagensNoticia($noticias['id_noticia'])[0]["caminho"]; ?>"
                onError="this.onerror=null;this.src='./imgs/img_padrao_noticia.png';" alt="Imagem da Notícia">
            </section>


            <section class="col-md-5">
                <h3> <?= $noticias['titulo_noticia']; ?> </h3>

                <p> <?= substr($noticias['texto_noticia'], 0, 500); ?> [...] </p>

                <a class="btn btn-primary" href="home.php?navegacao=ler_noticia&noticia=<?= $noticias['titulo_noticia']; ?>">Ler mais</i></a>

                <?php
                if ( isset($_SESSION['usuario']) && $_SESSION['usuario']['admin'] == true ) {
                    if ($noticias['cod_categoria'] != 5) {
                      echo "
                      <a class='btn btn-default' href="."./home.php?navegacao=atualizar_noticia&noticia=".$noticias['id_noticia'].">Atualizar</a>
                      <a class='btn btn-default' href="."./../controlador/desativar_noticia.php?id_noticia=".$noticias['id_noticia'].">Desativar</a>
                      ";
                    }else{
                      echo "
                      <a class='btn btn-default' href="."./home.php?navegacao=atualizar_noticia&noticia=".$noticias['id_noticia'].">Reativar</a>
                      ";
                    }
                }
                ?>

            </section>

        </section>
        <!-- /.row -->
        <hr>

        <?php endforeach; ?>

<p style="text-align: center;">Jesus te ama!</p>

        <!-- /.row -->
