<!DOCTYPE html>
<html lang="en">

<head>


    <title>Hora exata</title>


</head>

<body>

                <script type="text/javascript">

                    function _cpf(cpf) {
                        cpf = cpf.replace(/[^\d]+/g, '');
                        if (cpf == '') {
                            return false;
                        }
                        if (cpf.length != 11 ||
                            cpf == "00000000000" ||
                            cpf == "11111111111" ||
                            cpf == "22222222222" ||
                            cpf == "33333333333" ||
                            cpf == "44444444444" ||
                            cpf == "55555555555" ||
                            cpf == "66666666666" ||
                            cpf == "77777777777" ||
                            cpf == "88888888888" ||
                            cpf == "99999999999"){

                            return false;
                        }
                        add = 0;
                        for (i = 0; i < 9; i++)
                            add += parseInt(cpf.charAt(i)) * (10 - i);
                        rev = 11 - (add % 11);
                        if (rev == 10 || rev == 11)
                            rev = 0;
                        if (rev != parseInt(cpf.charAt(9)))
                            return false;
                        add = 0;
                        for (i = 0; i < 10; i++)
                            add += parseInt(cpf.charAt(i)) * (11 - i);
                        rev = 11 - (add % 11);
                        if (rev == 10 || rev == 11)
                            rev = 0;
                        if (rev != parseInt(cpf.charAt(10)))
                            return false;
                        return true;
                    }

                    function validarCPF(){
                        if($("#cpf").val()==""){
//tirar o alert e incuir a parte onde mexe na classe CSS
//                            alert('CPF em branco');
                            $("#cpf").addClass("parsley-error")
                            $('#msg_cpf').html('<ul class="parsley-errors-list filled" id="parsley-id-cpf"><li class="parsley-required">Valor obrigatório.</li></ul>');
                            $("#cpf").focus();
                            return false;
                        }else{
                            if(!_cpf($("#cpf").val())){
//                                alert('CPF inválio');
                                $("#cpf").addClass("parsley-error")
                                $('#msg_cpf').html('<ul class="parsley-errors-list filled" id="parsley-id-cpf"><li class="parsley-required">Formato inválido.</li></ul>');
                                $("#cpf").focus();
                                return false;
                            }
                            else{
                                $('#msg_cpf').html('')
                                return true;

                            }
                        }
                    }


                    function validarData(){
                        dt_nasc = new Date($('#dt_nasc').val());
                        hoje = new Date();
                        
                        if (dt_nasc > hoje){                    
                            $('#dt_nasc').focus();
                            $('#msg_data').html('<ul class="parsley-errors-list filled" id="parsley-id-cpf"><li class="parsley-required">Data inválida.</li></ul>');                            
                           return false;

                        }else{
                            $('#msg_data').html('');
                           return true;
                        }
                    }

                    function validarForm(){

                        if(validarCPF() && validarData()){
                            return true;
                        }else{
                            return false;
                        }
                    }

                </script>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cadastro de usuário</h1>

            </div>
        </div>

        <div class="row">
            <div class="col-md-8">

                <form data-parsley-validate name="sentMessage" id="cadastroForm" novalidate
                action="../controlador/formularios/processa_cadastro_usuario.php" method="post" onsubmit="return validarForm()">

                <div class="form-group">
                    <label>Nome completo:</label>
                    <input type="text" class="form-control" name="nome"
                    required data-parsley-pattern="^[a-zA-Z-A-Za-zÀ-ú ]+$"
                    placeholder="Preencha com seu nome completo, máximo de 50 digitos"/ minlength="3" maxlength="50">
                </div>


                <div class="form-group">
                    <label>CPF:</label>
                    <input name="cpf" type="text" class="form-control cpf" 
                    id="cpf" placeholder="11 digitos, sem pontos nem traço" maxlength="11">
                    <div id="msg_cpf"></div>
                </div>
                <input type="hidden" id="verifica_cpf" value="0"/>


                <div class="form-group">
                    <label>E-Mail:</label>
                    <input type="email" name="email" class="form-control" required data-parsley-type="email"
                    data-parsley-trigger="keyup" placeholder="Preencha com seu email, máximo de 50 digitos" maxlength="50" />
                </div>


                <div class="control-group form-group">
                    <div class="controls">
                        <label>Data de nascimento:</label>
                        <input type="Date" name="dt_nasc" class="form-control" id="dt_nasc" maxlength="10" required="email" onblur="validaData(this);">
                        <p class="help-block" ></p>
                        <div id="msg_data"></div>
                    </div>
                </div>

                



                <div class="control-group form-group">
                    <div class="controls">
                        <label>Senha:</label>
                        <input type="password" name="senha" class="form-control" id="senha"
                        required data-validation-required-message="Please enter your password." minlength="6"
                        data-parsley-trigger="keyup" placeholder="Mínimo de 6 caracteres" >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Confirmar senha:</label>
                        <input type="password" name="confirmar_senha" class="form-control" id="confirmar_senha"
                        required data-validation-required-message="Please enter your password confirmation."
                        required data-parsley-equalto="#senha" data-parsley-trigger="keyup"
                        placeholder="Repita a senha">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Fazer cadastro</button>
                    <button type="reset" class="btn btn-default m-l-5">Limpar campos</button>
                </div>

            </form>

            <hr>
            <h4>Já possui uma conta em nosso site? Faça já o <a href="home.php?navegacao=login">login</a>!</h4>

        </div>

    </div>

</div>

<script type="text/javascript">
  $('#cadastroForm').parsley();
</script>

</body>
</html>
