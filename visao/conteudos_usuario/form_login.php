<!DOCTYPE html>
<html lang="en">

<head>

   <style type="text/css">

    .wrapper{
        //padding-top: 20px;
        padding-top: 50px;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        border-color:#843534;
        box-shadow: none;
    }


    input.parsley-error:focus,
    select.parsley-error:focus,
    textarea.parsley-error:focus {
        border-color:#843534;
        box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #ce8483
    }


    .parsley-errors-list {
        list-style-type: none;
        opacity: 0;
        transition: all .3s ease-in;

        color: #d16e6c;
        margin-top: 5px;
        margin-bottom: 0;
        padding-left: 0;
    }

    .parsley-errors-list.filled {
        opacity: 1;
        color: #a94442;
    }

</style>
<title>Hora exata</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/modern-business.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>

<body>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Login</h1>

            </div>
        </div>
        <!-- /.row -->



        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-md-8">


                <form role="form" id="loginForm" name="login" action="../controlador/formularios/processa_login.php" method="post">

                    <div class="form-group">
                        <label for="exampleInputEmail1">
                            <i class="glyphicon glyphicon-envelope"></i> Email
                        </label>
                        <input type="email" name="email" class="form-control" required data-parsley-type="email" data-parsley-trigger="keyup" placeholder="Preencha com seu email" data-validation="email" data-validation-error-msg="Preencha com um e-mail válido"/>
                    </div>


                <div class="control-group form-group">
                    <label for="exampleInputPassword1">
                        <i class="glyphicon glyphicon-lock"></i> Senha
                    </label>
                     <div class="controls">
                    <input class="form-control" name="senha" type="password"
                    data-validation="length"
                    data-validation-length="min7"
                    data-validation-error-msg="Digite sua senha"
                    id="senha"
                    data-parsley-trigger="keyup"
                    required data-parsley-length="[6]"
                    placeholder="Preencha com sua senha"/>
                     <p class="help-block"></p>
                </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Logar</button>
                </div>


            </form>

            <br>
            <hr>
            <h4>Ainda não possui uma conta em nosso site? Faça já o seu <a href="home.php?navegacao=cadastro_usuario">cadastro</a>!</h4>


        </div>

    </div>
    <!-- /.row -->

    <script type="text/javascript">
      $('#loginForm').parsley();
  </script>


</div>
