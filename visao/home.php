<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jornal IFC</title>

    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
    <script type="text/jQuery" src="../bootstrap/js/dropdown.js"></script>
    <script src="../bootstrap/js/jquery-1.12.4-jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>



<!-- parada da validação parsley-->
    <script src="../bootstrap/parsleyjs/dist/parsley.js"></script>
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/cssparsley.css">

</head>

<body>
   <br>
   <header>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <section class="container">

            <!-- CHAMADA DO MENU -->

            <?php
            include_once '../controlador/menu.php';
            ?>

        </section>
    </nav>

</header>

<!-- Page Content -->
<br>
<section class="container">
    <!-- CHAMADA DAS NOTÍCIAS -->

    <?php

    switch (filter_input(INPUT_GET, 'navegacao'))
    {
        case "noticias":
        include ("conteudos_noticias/listar_noticias.php");
        break;
        case "ler_noticia":
        include ("conteudos_noticias/ler_noticia.php");
        break;
        case "pesquisas":
        include ("conteudos_noticias/pesquisas.php");
        break;
        case "contato":
        include ("conteudos_contatos/form_contato.php");
        break;
        case "cadastro_usuario":
        include ("conteudos_usuario/form_cadastro_usuario.php");
        break;
        case "login":
        include ("conteudos_usuario/form_login.php");
        break;
        case "detalhes_da_conta":
        include ("conteudos_conta/detalhes_da_conta.php");
        break;
        case "atualizar_informacoes":
        include ("conteudos_conta/form_atualizar_informacoes.php");
        break;
        case "alterar_senha":
        include ("conteudos_conta/form_alterar_senha.php");
        break;
        case "mensagens_enviadas":
        include ("conteudos_conta/mensagens_enviadas.php");
        break;
        case "logout":
        include ("conteudos_conta/logout.php");
        break;
        case "detalhes_da_conta_adm":
        include ("conteudos_admin/detalhes_da_conta_adm.php");
        break;
        case "cadastrar_noticias":
        include ("conteudos_admin/form_cadastro_noticias.php");
        break;
        case "atualizar_noticia":
        include ("conteudos_admin/form_atualizar_noticia.php");
        break;
        case "excluir_midia":
        include ("../controlador/excluir_midia.php");
        break;
        case "cadastrar_categorias":
        include ("conteudos_admin/form_cadastro_categorias.php");
        break;
        case "gerenciar_categorias":
        include ("conteudos_admin/gerenciar_categorias.php");
        break;
        case "atualizar_categoria":
        include ("conteudos_admin/form_atualizar_categoria.php");
        break;
        case "desativar_categoria":
        include ("../controlador/desativar_categoria.php");
        break;
        case "reativar_categoria":
        include ("../controlador/reativar_categoria.php");
        break;
        case "ver_mensagens":
        include ("conteudos_admin/mensagens_recebidas.php");
        break;
        case "ler_mensagem":
        include ("conteudos_contatos/ler_mensagem.php");
        break;
        case "responder_mensagem":
        include ("conteudos_admin/form_responder_mensagem.php");
        break;
        case "ver_usuarios":
        include ("conteudos_admin/usuarios_cadastrados.php");
        break;
        default:
        include ("conteudos_noticias/listar_noticias.php");
    }

    ?>

    <!-- Marketing Icons Section -->



    <!-- Footer -->
    <footer>
        <hr>
        <section class="row">
            <section class="col-lg-12">
              <p>Copyright &copy; - Lucas Martendal, Nicolas dos Santos Rosa & Gabriel Laurindo Tambosi - Projeto Final de Curso 2016-2017</p>
            </section>
        </section>
    </footer>

</section>
<!-- /.container -->

<!-- jQuery e bootstrap pra funfa o dropdown -->
<script src="../bootstrap/js/jquery.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- se excluir esse caraio buga o dropdown -->



</body>

</html>
