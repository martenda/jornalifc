<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoUsuario();

require_once './../controlador/controlador_mensagens.php';
?>


          <h1 class="page-header">Mensagens Enviadas</h1>



          <h4 class="sub-header">Aqui estão todos os seus contatos com nosso site:</h4>

          <?php
          if(obterMensagensEnviadas() == false){
            echo "<br>";
            echo "Você ainda não enviou nenhuma mensagem em nosso site...";
            echo "<a class='btn btn-success' href='home.php?navegacao=contato'>Entre em Contato</i></a>";
            exit;
          }
          ?>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th>Tipo de contato</th>
                  <th>Data do contato</th>
                  <th>Status</th>
                  <th>Opções</th>

                </tr>
              </thead>
              <tbody>

              <?php foreach (obterMensagensEnviadas() as $mensagens) :
                  $resposta = obterResposta($mensagens['id_contato']);
                  if($resposta == false){
                      $status = "Esperando resposta...";
                      $opcoes = "
                      <a href="."home.php?navegacao=ler_mensagem&contato={$mensagens['id_contato']}>Ler mensagem</a>
                      ";
                  }else{
                      $status = "Respondida em {$resposta[0]['data_contato']}";
                      $opcoes = "
                      <a href="."home.php?navegacao=ler_mensagem&contato={$mensagens['id_contato']}>Ler mensagem</a>
                      <br>
                      <a href="."home.php?navegacao=ler_mensagem&contato={$resposta[0]['id_contato']}>Ler resposta</a>
                      ";
                  }
              ?>

                <tr>

                  <td> <?= $mensagens['tipo_contato']; ?> </td>
                  <td> <?= $mensagens['data_contato']; ?> </td>
                  <td> <?= $status ?> </td>
                  <td> <?= $opcoes ?> </td>
                </tr>

              <?php endforeach; ?>


</tbody>
</table>
<a class="btn btn-success" href="home.php?navegacao=contato">Entre em Contato</i></a>
</div>
