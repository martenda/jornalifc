<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoUsuario();

?>
<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Atualização de senha de usuário</h1>

        </div>
    </div>
    <!-- /.row -->



    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
<div class="row">
    <div class="col-md-8">

  <h3>Atualize a senha de sua conta:</h3>

  <form name="sentMessage" data-parsley-validate name="sentMessage" novalidate id="cadastroForm" novalidate action="./../controlador/formularios/processa_atualiza_senha.php" method="post">


      <div class="control-group form-group">
          <div class="controls">
              <label>Senha atual:</label>
              <input type="password" name="senha_atual" class="form-control" id="senha_atual" 
              required data-validation-required-message="Please enter your password." minlength="6"
                        data-parsley-trigger="keyup" >
              <p class="help-block"></p>
          </div>
      </div>

      <div class="control-group form-group">
          <div class="controls">
              <label>Nova senha:</label>
              <input type="password" name="nova_senha" class="form-control" id="nova_senha"  required data-validation-required-message="Please enter your password." minlength="6"
                        data-parsley-trigger="keyup">
              <p class="help-block"></p>
          </div>
      </div>

      <div class="control-group form-group">
          <div class="controls">
              <label>Repita a nova senha:</label>
              <input type="password" name="confirmar_senha" class="form-control" id="confirmar_senha" r required data-validation-required-message="Please enter your password." minlength="6" required data-parsley-equalto="#nova_senha"
                        data-parsley-trigger="keyup">
              <p class="help-block"></p>
          </div>
      </div>



      
                     

      <div id="success"></div>
      <!-- For success/fail messages -->
      <button type="submit" class="btn btn-success">Atualizar</button>
  </form>

</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->
