<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoUsuario();

  require_once './../controlador/controlador_detalhes_conta.php';
  $detalhes = obterDetalhesConta($_SESSION['usuario']['id']);

?>

<script type="text/javascript">
   function validarData(){
                        dt_nasc = new Date($('#dt_nasc').val());
                        hoje = new Date();
                        
                        if (dt_nasc > hoje){                    
                            $('#dt_nasc').focus();
                            $('#msg_data').html('<ul class="parsley-errors-list filled" id="parsley-id-cpf"><li class="parsley-required">Data inválida.</li></ul>');                            
                           return false;

                        }else{
                            $('#msg_data').html('');
                           return true;
                        }
                    }

                    function validarForm(){

                        if(validarData()){
                            return true;
                        }else{
                            return false;
                        }
                    }

                </script>


<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Atualizar Informações</h1>
        </div>
    </div>
    <!-- /.row -->



    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">
        <div class="col-md-8">

            <h3>Atualize suas informações em nosso sistema:</h3>

            <form  data-parsley-validate name="sentMessage" name="sentMessage" id="cadastroForm" novalidate
                action="./../controlador/formularios/processa_atualiza_usuario.php" method="post" onsubmit="return validarForm()">

               <div class="form-group">
                        <label>Nome completo:</label>
                        <input type="text" name="nome" class="form-control" id="nome"
                        required data-parsley-pattern="^[a-zA-Z-A-Za-zÀ-ú ]+$" value="<?= $detalhes['nome']; ?>">
                        <p class="help-block"></p>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Email:</label>
                        <input type="email" name="email" class="form-control" id="email"
                            required data-validation-required-message="Please enter your email address."
                            value="<?= $_SESSION['usuario']['login']; ?>">
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Data de nascimento:</label>
                        <input type="date" name="dt_nasc" class="form-control" id="dt_nasc"
                            required data-validation-required-message="Please enter your name."
                            value="<?= $detalhes['dt_nasc']; ?>" onblur="validaData(this);">
                        <p class="help-block"></p>
                          <div id="msg_data"></div>
                    </div>
                </div>

 
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Atualizar</button>
                    <button type="reset" class="btn btn-default m-l-5">Resetar campos</button>
                </div>

            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<script type="text/javascript">
  $('#cadastroForm').parsley();
</script>
