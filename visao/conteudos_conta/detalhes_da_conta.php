<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoUsuario();

  require_once './../controlador/controlador_detalhes_conta.php';
  require_once './../modelo/login.class.php';
  $detalhes = obterDetalhesConta($_SESSION['usuario']['id']);
  $idade = calculaIdade($detalhes['dt_nasc']);
  $obj_login = new Login();
  $verificacao_admin = $obj_login -> verifica_admin_logado();
?>


          <h1 class="page-header">Detalhes da Conta</h1>
          <h4 class="sub-header">Estas são todas as suas informações salvas pelo nosso sistema:</h4>
          <div class="table-responsive">
            <label>Nome completo: <?= $detalhes['nome']; ?> </label><br>
            <label>CPF: <?= $detalhes['cpf']; ?></label><br>
            <label>Email: <?= $_SESSION['usuario']['login']; ?></label><br>
            <label>Idade: <?= $idade; ?></label><br>
            <br>
            <a href="./home.php?navegacao=atualizar_informacoes">Atualizar Informações</a><br>
            <a href="./home.php?navegacao=alterar_senha">Alterar Senha</a><br>
            <?php
            if ($verificacao_admin) {
                echo "
                <br>
                <label>Você é um administrador!</label>
                <br>
                <a href="."../controlador/deixar_admin.php".">Deixar de ser admin</a>
                <br>
                ";
            }
            ?>
          </div>
