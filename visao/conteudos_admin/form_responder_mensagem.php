<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoAdmin();

  require_once './../controlador/controlador_mensagens.php';

  $id_contato = filter_input(INPUT_GET, 'contato');
  $detalhes = obterDetalhesContato($id_contato);

?>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Responder Mensagem</h1>
        </div>
    </div>
    <!-- /.row -->



    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">

        <div class="col-md-8">

            <h3>Detalhes da mensagem:</h3>

            <p><i class="fa fa-phone"></i>
                <label title="Phone">#</label><?= $detalhes['id_contato'] ?>
            </p>
            <p><i class="fa fa-envelope-o"></i>
                <label title="Email">Nome</label>: <?= obterNomeContato($detalhes['id_usuario']); ?>
            </p>
            <p><i class="fa fa-clock-o"></i>
                <label title="Hours">E-mail</label>: <?= obterEmailContato($detalhes['id_usuario']); ?>
            </p>
            <p><i class="fa fa-phone"></i>
                <label title="Phone">Tipo de contato</label>: <?= $detalhes['tipo_contato'] ?>
            </p>
            <p><i class="fa fa-clock-o"></i>
                <label title="Hours">Data do contato</label>: <?= $detalhes['data_contato'] ?>
            </p>
            <p><i class="fa fa-envelope-o"></i>
              <label title="Email">Texto</label>:
              <p><?= $detalhes['texto_contato'] ?></p>
            </p>

        </div>

        <div class="col-md-8">
            <h3>Resposta da mensagem:</h3>

            <form name="sentMessage" id="contactForm" novalidate action="../controlador/formularios/processa_resposta.php" method="post">

              <input type="hidden" name="id_contato" value="<?= $id_contato ?>">

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Tipo da resposta:</label>
                        <input type="text" name="tipo_resposta" class="form-control" id="motivo_contato" equired data-validation-required-message="Please enter your message"
                        placeholder="Esclarecimento de dúvida, resolução do problema, discordância de opinião, agradecimento..." data-parsley-length="[5, 100]" data-parsley-required data-parsley-required="true" >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Resposta:</label>
                        <textarea name="resposta" rows="10" cols="100" class="form-control" id="mensagem" required data-validation-required-message="Please enter your message" data-parsley-length="[10, 5000]" style="resize:none"></textarea>
                    </div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-success">Enviar resposta</button>

            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
    <script type="text/javascript">
      $('#contactForm').parsley();
  </script>