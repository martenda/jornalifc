<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoAdmin();
?>
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cadastro de categoria de notícias</h1>

            </div>
        </div>
        <!-- /.row -->

        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-md-8">


                <form name="sentMessage" data-parsley-validate name="sentMessage" id="contactForm" novalidate action="./../controlador/formularios/processa_cadastro_categorias.php" method="post">

                    <div class="form-group">
                            <label class="col-sm-3 control-label">Título da categoria:</label>
                            <input type="text" name="titulo" class="form-control" id="titulo" required data-validation-required-message="Please enter your name." required data-parsley-pattern="^[a-zA-Z ]+$" minlength="6">
                            <p class="help-block"></p>
                    </div>

                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-success">Cadastrar Categoria</button>
                </form>
            </div>

        </div>
        <!-- /.row -->


    </div>
  