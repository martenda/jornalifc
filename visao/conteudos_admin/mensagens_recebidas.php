<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoAdmin();

require_once './../controlador/controlador_mensagens.php';
?>

          <h1 class="page-header">Mensagens Recebidas</h1>

          <h4 class="sub-header">Aqui estão todas as mensagens enviadas pelos usuários:</h4>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th> # </th>
                  <th>Nome</th>
                  <th>Tipo de contato</th>
                  <th>Data do contato</th>
                  <th>Status</th>
                  <th>Opções</th>

                </tr>
              </thead>
              <tbody>

              <?php
                  foreach (obterMensagensRecebidas() as $mensagens) :

                  $resposta = obterResposta($mensagens['id_contato']);
                  if($resposta == false){
                      $status = "A responder...";
                      $opcoes = "
                      <a href="."home.php?navegacao=ler_mensagem&contato={$mensagens['id_contato']}>Ler mensagem</a>
                      <br>
                      <a href="."home.php?navegacao=responder_mensagem&contato={$mensagens['id_contato']}>Responder</a>
                      ";
                  }else{
                      $status = "Respondida em {$resposta[0]['data_contato']}";
                      $opcoes = "
                      <a href="."home.php?navegacao=ler_mensagem&contato={$mensagens['id_contato']}>Ler mensagem</a>
                      <br>
                      <a href="."home.php?navegacao=ler_mensagem&contato={$resposta[0]['id_contato']}>Ler resposta</a>
                      ";
                  }
              ?>

                <tr class="">
                    <td> <?= $mensagens['id_contato']; ?> </td>
                    <td> <?= obterNomeContato($mensagens['id_usuario']); ?> </td>
                    <td> <?= $mensagens['tipo_contato']; ?> </td>
                    <td> <?= $mensagens['data_contato']; ?> </td>
                    <td> <?= $status ?> </td>
                    <td> <?= $opcoes ?> </td>
                </tr>

              <?php endforeach; ?>


</tbody>
</table>
</div>
