<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoAdmin();

  include_once '../controlador/controlador_categorias.php';
?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cadastro de notícias</h1>

            </div>
        </div>
        <!-- /.row -->

        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-md-8">
                <h3>Insira as informações da nova notícia</h3>


                <form name="sentMessage" id="contactForm" data-parsley-validate name="sentMessage" novalidate action="./../controlador/formularios/processa_cadastro_noticias.php" method="post" enctype="multipart/form-data">

                    <div class="control-group form-group">
                            <label>Título:</label>
                            <input type="text" name="titulo" class="form-control" id="titulo" required data-validation-required-message="Please enter your name." required data-parsley-required data-parsley-required="true" data-parsley-length="[10, 100]">
                            <p class="help-block"></p>
                    </div>

                    <div class="control-group form-group">
                            <label>Texto:</label>
                            <textarea name="texto" rows="25" cols="100" class="form-control" id="texto" required data-validation-required-message="Please enter your message" style="resize:none" required data-parsley-required data-parsley-required="true" data-parsley-length="[10, 5000]" maxlength="5001"></textarea>
                    </div>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Tag de referência:</label>
                            <input type="text" name="tag" class="form-control" id="tag">
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Categoria:</label>
                            <select name="categoria" class="form-control" id="categoria" required data-validation-required-message="Please enter your name.">
                              <?php foreach (obterCategoriasAtivas() as $categorias) : ?>
                                <option value="<?= $categorias['cod_categoria']; ?>" class="form-control"><?= $categorias['desc_categoria']; ?></option>
                              <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Imagens:</label>
                            <input type="file" name="arquivos[]" class="form-control" id="arquivo" multiple="multiple">
                            <p class="help-block">Tipo de arquivo: png, jpg ou gif;<br>Tamanho máximo do arquivo: 2MB;<br>Nome do arquivo sem caracteres especiais!</p>
                        </div>
                    </div>

                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-success">Cadastrar Notícia</button>
                </form>
            </div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->
