<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoAdmin();

require_once './../controlador/controlador_categorias.php';
?>

          <h1 class="page-header">Gerenciar Categorias</h1>

          <h4 class="sub-header">Estas são todas as categorias cadastradas no sistema, e a quantidade de notícias em cada:</h4>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th>Código</th>
                  <th>Descrição</th>
                  <th>Número de Notícias</th>
                  <th>Status</th>
                  <th>Atualizar</th>

                </tr>
              </thead>
              <tbody>

              <?php foreach (obterTodasCategorias() as $categorias) : ?>

                <tr>

                  <td> <?= $categorias['cod_categoria'] ?> </td>
                  <td> <?= $categorias['desc_categoria'] ?> </td>
                  <td> <?= contaNoticias($categorias['cod_categoria'])[0]["count(id_noticia)"]; ?> </td>
                  <td>
                    <?php
                    if ( $categorias['status'] == 1 ) {
                        echo "Ativada";
                    } else {
                        echo "Desativada";
                    }
                    ?>
                  </td>
                  <td>
                    <a href="./home.php?navegacao=atualizar_categoria&categoria=<?= $categorias['cod_categoria']; ?>">Alterar nome da categoria</a>
                    <br>
                    <?php
                    if ( $categorias['status'] == 1 ) {
                        echo "
                        <a href="."./home.php?navegacao=desativar_categoria&categoria=".$categorias['cod_categoria'].">Desativar notícias da categoria</a>
                        ";
                    } else {
                      echo "
                      <a href="."./home.php?navegacao=reativar_categoria&categoria=".$categorias['cod_categoria'].">Reativar notícias da categoria</a>
                      ";
                    }
                    ?>
                  </td>

                </tr>

              <?php endforeach; ?>


</tbody>
</table>
<a class="btn btn-success" href="home.php?navegacao=cadastrar_categorias">Cadastrar Categoria</i></a>
</div>
