<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoAdmin();

  require_once './../controlador/controlador_detalhes_noticia.php';
  require_once './../controlador/controlador_noticias.php';

  $id_noticia = filter_input(INPUT_GET, 'noticia');
  $detalhes = obterDetalhesNoticia($id_noticia);

?>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Atualizar as Informaçẽs dessa Notícia</h1>
        </div>
    </div>
    <!-- /.row -->



    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">
        <div class="col-md-8">

            <h3>Atualize a notícia:</h3>

            <form name="sentMessage" id="cadastroForm" novalidate action="./../controlador/formularios/processa_atualiza_noticia.php?noticia=<?= $id_noticia ?>" method="post" enctype="multipart/form-data" data-parsley-validate name="sentMessage">

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Título:</label>
                        <input type="text" name="titulo" class="form-control" id="titulo" required data-parsley-required data-parsley-required="true" data-parsley-length="[10, 100]" maxlength="101"
                        value="<?= $detalhes['titulo_noticia']; ?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Texto:</label>
                        <textarea name="texto" rows="25" cols="100" class="form-control" id="texto" required data-parsley-required data-parsley-required="true" data-parsley-length="[10, 5000]" maxlength="5001"
                        style="resize:none"><?= $detalhes['texto_noticia']; ?></textarea>
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Tag de referência:</label>
                        <input type="text" name="tag" class="form-control" id="tag" required data-validation-required-message="Please enter your email address."
                        required data-validation-required-message="Please enter your name." required data-parsley-required data-parsley-required="true" data-parsley-length="[3, 100]"
                        value="<?= $detalhes['tags_referencia']; ?>">
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Categoria:</label>
                        <select name="categoria" class="form-control" id="categoria" required data-validation-required-message="Please enter your name.">
                          <?php foreach (obterCategoriasAtivas() as $categorias) : ?>
                            <option value="<?= $categorias['cod_categoria']; ?>" class="form-control" <?php if ($categorias['cod_categoria'] == $detalhes['cod_categoria']) {
                              echo "selected=true"; } ?> ><?= $categorias['desc_categoria']; ?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="">
                    <div class="controls">
                        <label>Imagens:</label>
                        <input type="file" name="arquivos[]" class="form-control" id="arquivo" multiple="multiple">
                        <p class="help-block">Tipo de arquivo: png, jpg ou gif;<br>Tamanho máximo do arquivo: 2MB;<br>Nome do arquivo sem caracteres especiais!</p>
                    </div>
                </div>



                <div class="row">
                    <?php foreach (obterImagensNoticia($id_noticia) as $imagens) : ?>

                    <section class="col-md-6">
                      <img class="img-responsive img-hover" src="./../<?= $imagens['caminho']; ?>" alt="Imagem da Notícia">
                      <p><?= $imagens['titulo']; ?></p>
                      <p><a class="btn" href="./home.php?navegacao=excluir_midia&cod_midia=<?=$imagens['cod_midia']?>">Excluir Mídia</a></p>
                    </section>

                    <?php endforeach; ?>
                </div>




                <div id="success"></div>
                <button type="submit" class="btn btn-success">Atualizar Notícia</button>

            </form>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
