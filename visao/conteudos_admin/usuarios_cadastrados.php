<?php
require_once './../controlador/permite_acesso.php';
permiteAcessoAdmin();

require_once './../controlador/controlador_usuarios.php';
require_once './../controlador/controlador_detalhes_conta.php';
?>


          <h1 class="page-header">Usuários Cadastrados</h1>



          <h4 class="sub-header">Estes são todos os usuários cadastrados no sistema:</h4>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th>ID</th>
                  <th>Nome Completo</th>
                  <th>E-mail</th>
                  <th>CPF</th>
                  <th>Idade</th>
                  <th>Tipo de Usuário</th>

                </tr>
              </thead>
              <tbody>

              <?php foreach (obterTodosUsuarios() as $usuarios) : ?>

                <tr>

                  <td> <?= $usuarios['id_usuario']; ?> </td>
                  <td> <?= $usuarios['nome']; ?> </td>
                  <td> <?= $usuarios['email']; ?> </td>
                  <td> <?= $usuarios['cpf']; ?> </td>
                  <td> <?= calculaIdade($usuarios['dt_nasc']); ?> </td>
                  <td>
                    <?php
                    if ( $usuarios['id_tipo_usuario'] == 1 ) {
                        echo "Administrador";
                    } else {
                        echo "<a href="."./../controlador/tornar_admin.php?id_usuario=".$usuarios['id_usuario'].">Tornar Admin</a>";
                    }
                    ?>
                  </td>

                </tr>

              <?php endforeach; ?>


</tbody>
</table>
</div>
