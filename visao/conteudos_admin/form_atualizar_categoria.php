<?php
  require_once './../controlador/permite_acesso.php';
  permiteAcessoAdmin();

  require_once './../controlador/controlador_categorias.php';

  $cod_cat = filter_input(INPUT_GET, 'categoria');
  $detalhes = obterDetalhesCategoria($cod_cat);

?>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Atualizar as Informaçẽs dessa Categoria</h1>
        </div>
    </div>
    <!-- /.row -->



    <!-- Contact Form -->
    <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <div class="row">
        <div class="col-md-8">

            <h3>Atualize a categoria:</h3>

            <form data-parsley-validate name="sentMessage" id="cadastroForm" novalidate action="./../controlador/formularios/processa_atualiza_categoria.php?categoria=<?= $cod_cat ?>" method="post">


                <div class="control-group form-group">
                    <div class="controls">
                        <label>Descrição:</label>
                        <input type="text" name="desc" class="form-control" id="desc"  required data-parsley-pattern="^[a-zA-Z-A-Za-zÀ-ú ]+$"
                        placeholder="Preencha com seu nome completo" maxlength="20" minlength="5"
                        value="<?= $detalhes[0]['desc_categoria']; ?>">
                        <p class="help-block"></p>
                    </div>
                </div>


                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-success">Atualizar Categoria</button>

            </form>

        </div>

    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
