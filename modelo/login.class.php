<?php

  class Login {

    function __construct(){
        if( !isset($_SESSION) ){
            session_start();
        }
    }

    function fazer_login($email, $senha)
    {
        $conn = Database::getConnection();

        $consulta = "SELECT id_usuario, id_tipo_usuario FROM usuario WHERE email = '$email' AND senha = '$senha'";
        $stmt     = $conn->query($consulta);
        $usuario  = $stmt->fetch(PDO::FETCH_OBJ);

        if ( !isset($usuario) || empty($usuario))
        {
            echo("<br>
                  <script type='text/javascript'>
                  swal(
                      {
                          title: 'Erro!',
                          text: 'Login ou senha incorretos! Tente novamente :)',
                          type: 'error',
                          showCancelButton: false,
                          confirmButtonClass: 'btn-succes',
                          confirmButtonText: 'Ok'
                      },
                      function(){
                          location.href='../../visao/home.php?navegacao=login';
                      }
                  );
                  </script>");
        } else{

            $_SESSION['usuario']['id']   = $usuario->id_usuario;
            $_SESSION['usuario']["login"] = $email;
            $_SESSION['usuario']['esta_logado'] = true;

            if ($usuario->id_tipo_usuario == "1") {
              $_SESSION['usuario']['admin'] = true;
            } else {
              $_SESSION['usuario']['admin'] = false;
            }

            header('location:../../visao/home.php');

        }
    }

    function fazer_logout()
    {
      $_SESSION = array();
      header('location:../../visao/home.php');
    }

    function verifica_logado(){

        if (!isset($_SESSION['usuario']) OR $_SESSION['usuario']['esta_logado'] == false)
        {
            return false;
        }else {
            return true;
        }
    }

    function verifica_admin_logado(){

        if (!isset($_SESSION['usuario']) OR $_SESSION['usuario']['admin'] == false)
        {
            return false;
        }else {
            return true;
        }
    }

  }
